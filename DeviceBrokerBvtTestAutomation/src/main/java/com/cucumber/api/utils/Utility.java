package com.cucumber.api.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * @author gsachin
 *
 */
public class Utility {

	private static String file_path = "";

	/*
	 * Set file path when test scripts are executed from batch file
	*/
	public static void setFilePath(String path) throws Exception {
		file_path = path;
	}

	/*
	 * Read json file and return as a string
	*/
	public static String generateStringFromResource(String path) throws Exception {
		String value = "";
		if (file_path.equals("")) {
			value = generateStringFromResourceForTest(path);
		} else {
			value = generateStringFromResourceForJar(path);
		}

		return value;
	}

	/*
	 * Get property value for properties defined in api.properties file.
	*/
	public static String getPropertyValue(String propertyName) throws IOException {
		String propertyValue = "";
		if (file_path.equals("")) {
			propertyValue = getPropertyValueForTest(propertyName);
		} else {
			propertyValue = getPropertyValueForJar(propertyName);
		}

		return propertyValue;
	}
	
	/*
	 * Get message property value for properties defined in message.properties file
	*/
	public static String getMessagePropertyValue(String messagePropertyName) throws IOException {
		String messagePropertyValue = "";
		if (file_path.equals("")) {
			messagePropertyValue = getMessagePropertyValueForTest(messagePropertyName);
		} else {
			messagePropertyValue = getMessagePropertyValueForJar(messagePropertyName);
		}

		return messagePropertyValue;
	}

	/*
	 * Read json file and return as a string when scripts are executed from batch file.
	*/
	public static String generateStringFromResourceForJar(String path) throws Exception {

		String rootPath = getParentFolderPath();

		FileInputStream in = new FileInputStream(rootPath + path);

		String value = new BufferedReader(new InputStreamReader(in)).lines().collect(Collectors.joining("\n"));
		in.close();
		System.out.print("\n JSON Input == \n" + value + "\n");
		return value;
	}

	/*
	 * Get property value for properties defined in api.properties file when scripts are executed from batch file.
	*/
	public static String getPropertyValueForJar(String propertyName) throws IOException {

		Properties prop = new Properties();

		String propertiesPath = getParentFolderPath();

		prop.load(new FileInputStream(propertiesPath + "/properties/api.properties"));

		return prop.getProperty(propertyName).trim();
	}

	/*
	 * Get absolute parent folder path when scripts are executed from batch file.
	*/
	private static String getParentFolderPath() {

		File jarPath = new File(Utility.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		return jarPath.getParentFile().getParentFile().getAbsolutePath();

	}

	/*
	 * Get message property value for properties defined in message.properties file when scripts are executed from batch file.
	*/
	public static String getMessagePropertyValueForJar(String messagePropertyName) throws IOException {
		Properties prop = new Properties();

		String propertiesPath = getParentFolderPath();

		System.out.println(" PROPERTY PATH----------   " + propertiesPath + "/properties/message.properties");

		prop.load(new FileInputStream(propertiesPath + "/properties/message.properties"));

		return prop.getProperty(messagePropertyName).trim();
	}

	/*
	 * Read json file and return as a string
	*/
	public static String generateStringFromResourceForTest(String path) throws Exception {

		FileInputStream in = new FileInputStream(ClassLoader.getSystemClassLoader().getResource(".").getPath() + path);
		String value = new BufferedReader(new InputStreamReader(in)).lines().collect(Collectors.joining("\n"));
		in.close();
		return value;
	}

	/*
	 * Get property value for properties defined in api.properties file when scripts are executed using mvn test or run from eclipse.
	*/
	public static String getPropertyValueForTest(String propertyName) throws IOException {
		String path = ClassLoader.getSystemClassLoader().getResource(".").getPath() + "/properties/api.properties";
		Properties prop = new Properties();
		prop.load(new FileInputStream(path));
		return prop.getProperty(propertyName).trim();
	}

	/*
	 * Get property value for properties defined in message.properties file when scripts are executed using mvn test or run from eclipse.
	*/
	public static String getMessagePropertyValueForTest(String messagePropertyName) throws IOException {
		String path = ClassLoader.getSystemClassLoader().getResource(".").getPath() + "/properties/message.properties";
		Properties prop = new Properties();
		prop.load(new FileInputStream(path));
		return prop.getProperty(messagePropertyName).trim();
	}
}
