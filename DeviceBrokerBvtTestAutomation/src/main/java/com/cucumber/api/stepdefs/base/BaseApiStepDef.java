package com.cucumber.api.stepdefs.base;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.cucumber.api.utils.Utility;

import io.cucumber.datatable.DataTable;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author gsachin
 *
 */
public class BaseApiStepDef {

	protected static Response response;
	private final String host_name = "hostname";
	private final String port = "port";
	private final String protocol = "protocol";
	private final String mqtt_port = "mqtt_port";
	private final String mqtt_protocol = "mqtt_protocol";
	private final String mqtt_client_id = "mqtt_client_id";
	private final String connect_disconnect_url = "connect_disconnect_url";
	private final String print_receipt_api_url = "print_receipt_api_url";
	private final String begin_end_deposit_api_url = "begin_end_deposit_api_url";
	private final String dispense_cash_api_url = "dispense_cash_api_url";
	private final String get_rfid_api_url = "get_rfid_api_url";
	private final String get_property_api_url = "get_property_api_url";
	private final String get_read_cash_count_api_url = "get_read_cash_count_api_url";
	private final String device_name_placeholder = "deviceNamePlaceholder";
	private final String terminal_id_place_holder = "terminalIdPlaceHolder";
	private final String property_name_place_holder = "propertyNamePlaceholder";
	private final String get_capability_api_url = "get_capability_api_url";
	private final String capabilityNamePlaceholder = "capabilityNamePlaceholder";
	private final String device_admin_api_url = "device_admin_api_url";
	private final String set_property_api_url = "set_property_api_url";
	protected final String content_type_value = "application/json";
	protected final String accept_value = "application/json";
	protected static String session_id = null;
	protected static Integer sleep = 300;
	protected final String content_type = "Content-Type";
	
	//input output json parameters
	private final String terminalID = "terminalID";
	private final String category = "category";
	private final String logicalName = "logicalName";
	private final String operationType = "operationType";
	private final String state = "state";
	private final String cashCounts = "cashCounts";
	private final String filterId = "filterId";
	private final String filterMask = "filterMask";
	private final String sourceID = "sourceID";
	private final String destID = "destID";
	protected final String additionalData = "additionalData";
	protected final String status_line = "Status line";
	protected final String result_code = "resultCode";
	protected final String status_code = "Status Code";
	protected final String type = "type";
	protected final String error_code = "errorCode";
	protected final String extended_error_code = "extendedErrorCode";
	protected final String message = "message";
	protected final String data = "data";
	
	
	// place holder
	private final String terminalIdPlaceHolder = "terminalIdPlaceHolder";
	private final String categoryPlaceHolder = "categoryPlaceHolder";
	private final String logicalNamePlaceHolder = "logicalNamePlaceHolder";
	private final String operationTypePlaceHolder = "operationTypePlaceHolder";
	private final String filterIdPlaceHolder = "filterIdPlaceHolder";
	private final String filterMaskPlaceHolder = "filterMaskPlaceHolder";
	private final String sourceIDPlaceHolder = "sourceIDPlaceHolder";
	private final String destIDPlaceHolder = "destIDPlaceHolder";
	private final String stateValuePlaceHolder = "2222";
	private final String cashCountsValuePlaceHolder = "cashCountsPlaceHolder";
	
	//json path
	private final String rfid_read_tag_response_success_message_json_path = "actions.actionBody.readTag.message[0]";
	private final String rfid_write_tag_response_success_message_json_path = "actions.actionBody.writeTagId.message[0]";
	private final String rfid_start_read_tag_response_success_message_json_path = "actions.actionBody.startReadTag.message[0]";
	private final String rfid_stop_read_tag_response_success_message_json_path = "actions.actionBody.stopReadTag.message[0]";
	private final String cc_begin_deposit_response_success_message_json_path = "actions.actionBody.beginDeposit.message[0]";
	private final String cc_end_deposit_response_success_message_json_path = "actions.actionBody.endDeposit.message[0]";
	protected final String api_response_result_code_json_path = "actions.actionBody.actionInfo.resultCode[0]";
	protected final String api_response_directio_additional_data_json_path = "actions.actionBody.directIO.additionalData[0][0]";
	protected final String api_response_directio_data_json_path = "actions.actionBody.directIO.data[0][0]";
	
	protected final String readTagOperation = "readTag";
	protected final String startReadTagOperation = "startReadTag";
	protected final String stopReadTagOperation = "stopReadTag";
	protected final String writeTagIdOperation = "writeTagId";
	protected final String beginDepositOperation = "beginDeposit";
	protected final String endDepositOperation = "endDeposit";
	
	
	/*
	 * Construct and return the base API url in format <protocol://<IP>:<port>.
	*/
	public String getBaseUrl() throws Throwable {

		String httpProtocol = Utility.getPropertyValue(protocol);
		String hostName = Utility.getPropertyValue(host_name);
		String httpPort = Utility.getPropertyValue(port);

		String base_url = httpProtocol + "://" + hostName + ":" + httpPort;

		return base_url;
	}
	
	/*
	 * Construct and return the base Messaging queue url in format <protocol://<IP>:<port>.
	*/
	public String getMqttUrl() throws Throwable {

		String message_protocol = Utility.getPropertyValue(mqtt_protocol);
		String hostName = Utility.getPropertyValue(host_name);
		String mqttPort = Utility.getPropertyValue(mqtt_port);
		String mqtt_url = message_protocol + "://" + hostName + ":" + mqttPort;
		System.out.println("MQTT URL = : " + mqtt_url);
		return mqtt_url;
	}

	/*
	 * Get url for Connect/Disconnect API
	*/
	public String getConnectDisconnectUrl() throws Throwable {

		return Utility.getPropertyValue(connect_disconnect_url);
	}

	/*
	 * Get url for Print API
	*/
	public String getPrintReceiptUrl() throws Throwable {

		return Utility.getPropertyValue(print_receipt_api_url);
	}
	
	/*
	 * Get url for Begin Deposit API
	*/
	public String getBeginEndDepositUrl() throws Throwable {

		return Utility.getPropertyValue(begin_end_deposit_api_url);
	}
	
	/*
	 * Get url for Dispense Cash API
	*/
	public String getDispenseCashUrl() throws Throwable {

		return Utility.getPropertyValue(dispense_cash_api_url);
	}
	
	/*
	 * Get url for RFID API's.
	*/
	public String getRfidApiUrl() throws Throwable {

		return Utility.getPropertyValue(get_rfid_api_url);
	}
	
	/*
	 * Get url for Get Property API.
	*/
	public String getPropertyUrl(String propertyNames, String deviceName, String terminalId) throws Throwable {
		
		String property_url ="";
		
		if (propertyNames.equalsIgnoreCase("all"))
			property_url = Utility.getPropertyValue(get_property_api_url).replace(device_name_placeholder, deviceName).replace(terminal_id_place_holder, terminalId).replace(property_name_place_holder, "");
		else
			property_url = Utility.getPropertyValue(get_property_api_url).replace(device_name_placeholder, deviceName).replace(terminal_id_place_holder, terminalId).replace(property_name_place_holder, propertyNames);

		return property_url;
	}
	
	/*
	 * Get url for Read Cash Count API.
	*/
	public String getReadCashCountUrl(String terminalId) throws Throwable {
		
		String property_url ="";
		
		property_url = Utility.getPropertyValue(get_read_cash_count_api_url).replace(terminal_id_place_holder, terminalId);
		
		return property_url;
	}
	
	/*
	 * Get url for Get Capability API.
	*/
	public String getCapabilityUrl(String capabilityNames, String deviceName) throws Throwable {
		
			String property_url ="";
			
			if (capabilityNames.equalsIgnoreCase("all"))
				property_url = Utility.getPropertyValue(get_capability_api_url).replace(device_name_placeholder, deviceName).replace(capabilityNamePlaceholder, "");
			else
				property_url = Utility.getPropertyValue(get_capability_api_url).replace(device_name_placeholder, deviceName).replace(capabilityNamePlaceholder, capabilityNames);

			return property_url;
	}
	
	/*
	 * Get url for Device Admin API.
	*/
	public String getDeviceAdminUrl() throws Throwable {
		
		return Utility.getPropertyValue(device_admin_api_url);
	}
	
	/*
	 * Get url for Set Property API.
	*/
	public String getSetPropertyUrl() throws Throwable {
		
		return Utility.getPropertyValue(set_property_api_url);
	}

	/*
	 * Get ClientID Property value.
	*/
	public String getMqttClientId() throws Throwable {

		return Utility.getPropertyValue(mqtt_client_id);
	}

	protected void callSetPropertyApi(String inputJson) throws Throwable {
		
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		// Check if session_id is null.
		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}

		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(inputJson);

		// Call API service.
		response = httpRequest.put(getSetPropertyUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	protected void callDirectIoApi(String inputJson) throws Throwable {
		
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		// Check if session_id is null.
		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}

		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(inputJson);

		// Call API service.
		response = httpRequest.post(getConnectDisconnectUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	protected String prepareInputJsonRequest(String jsonTemplate, DataTable dt) throws Throwable  {
		String inputJson = jsonTemplate;
		List<List<String>> rows = dt.asLists(String.class);

		for (List<String> columns : rows) {

			if (!columns.get(0).trim().equals("Field")) {

				if (columns.get(0).equals(terminalID)) {
					inputJson=inputJson.replace(terminalIdPlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(category)) {
					inputJson=inputJson.replace(categoryPlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(logicalName)) {
					inputJson=inputJson.replace(logicalNamePlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(operationType)) {
					inputJson=inputJson.replace(operationTypePlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(state)) {
					inputJson=inputJson.replace(stateValuePlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(cashCounts)) {
					inputJson=inputJson.replace(cashCountsValuePlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(filterId)) {
					inputJson=inputJson.replace(filterIdPlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(filterMask)) {
					inputJson=inputJson.replace(filterMaskPlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(sourceID)) {
					inputJson=inputJson.replace(sourceIDPlaceHolder, columns.get(1));
					
				} else if (columns.get(0).equals(destID)) {
					inputJson=inputJson.replace(destIDPlaceHolder, columns.get(1));
					
				}					
			}
		}
		
		return inputJson;
	}
	
	protected void verifyDeviceSpecificApiResponse(String operationType, DataTable dt) throws Throwable  {
		List<List<String>> rows = dt.asLists(String.class);

		for (List<String> columns : rows) {

			if (!columns.get(0).trim().equals("Field")) {

				if (columns.get(0).equals(message)) {
					
					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(columns.get(1)),
							response.jsonPath().get(getXpathValue(operationType)));
					
				}
				
				if (columns.get(0).equals(data)) {

					assertEquals("Response data did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_directio_data_json_path).toString());
				}
				
				if (columns.get(0).equals(additionalData)) {

					assertEquals("Response additional data did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_directio_additional_data_json_path));
				}
								
				if (columns.get(0).equals(status_code)) {

					assertEquals("Response code did not matched : ", columns.get(1),
							String.valueOf(response.getStatusCode()));
				}

				if (columns.get(0).equals(content_type)) {

					assertEquals("Content Type did not matched : ", columns.get(1), response.header(content_type));
				}

				if (columns.get(0).equals(status_line)) {

					assertEquals("Status line did not matched : ", columns.get(1), response.getStatusLine().trim());
				}

				if (columns.get(0).equals(result_code)) {

					assertEquals("resultCode did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_result_code_json_path));
				}

			}
		}
		RestAssured.reset();
		Thread.sleep(sleep);
	}
	
	private String getXpathValue(String operationType) {
		String xPath = "";
		
		if (operationType.equals(readTagOperation)) {
			xPath = rfid_read_tag_response_success_message_json_path;
		} else if(operationType.equals(writeTagIdOperation)){
			xPath = rfid_write_tag_response_success_message_json_path;
		} else if(operationType.equals(startReadTagOperation)){
			xPath = rfid_start_read_tag_response_success_message_json_path;
		} else if(operationType.equals(stopReadTagOperation)){
			xPath = rfid_stop_read_tag_response_success_message_json_path;
		} else if(operationType.equals(beginDepositOperation)){
			xPath = cc_begin_deposit_response_success_message_json_path;
		} else if(operationType.equals(endDepositOperation)){
			xPath = cc_end_deposit_response_success_message_json_path;
		}
		return xPath;
	}
	
	/*
	 * Common method for calling POST method for API.
	*/
	protected void callPostApi(String inputJson, String apiUrl) throws Throwable {
		
		RequestSpecification httpRequest;
		
		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}
		
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(inputJson);

		// Call API service.
		response = httpRequest.post(apiUrl);

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
}
