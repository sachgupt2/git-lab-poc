package com.cucumber.api.stepdefs.common;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;
import com.cucumber.api.utils.Utility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

/**
 * @author gsachin
 *
 */
/*
 * This class contain common step definitions for all devices.
 */
public class CommonStepDef extends BaseApiStepDef {

	private final String api_response_success_message_json_path = "actions.actionBody.actionInfo.message[0]";
	private final String api_response_error_message_json_path = "actions.actionBody.message[0]";
	
	private final String api_response_error_code_json_path = "actions.actionBody.errorCode[0]";
	private final String api_response_extended_error_code_json_path = "actions.actionBody.extendedErrorCode[0]";
	private final String api_response_type_json_path = "actions.actionBody.type[0]";
	private final String api_response_directio_data_json_path = "actions.actionBody.directIO.data[0][0]";
	
	private final String printer_property_not_supported = "PrinterPropertyNotSupported";
	private final String cc_property_not_supported = "CashChangerPropertyNotSupported";
	private final String printer_set_read_only_property = "printer-set-read-only-property";
	private final String rfid_set_read_only_property = "rfid-set-read-only-property";
	private final String cc_set_read_only_property = "cc-set-read-only-property";
	private final String printer_set_invalid_property_value = "printer-set-invalid-property-value";
	private final String cc_set_invalid_property_value = "cc-set-invalid-property-value";
	
	private final String printer_property_not_supported_param_name = "PRINTER_PROPERTY_NOT_SUPPORTED_ERROR_MESSAGE";
	private final String cc_property_not_supported_param_name = "CC_PROPERTY_NOT_SUPPORTED_ERROR_MESSAGE";
	private final String printer_set_read_only_property_param_name = "PRINTER_SET_PROPERTY_READ_ONLY_ERROR_MESSAGE";
	private final String rfid_set_read_only_property_param_name = "RFID_SET_PROPERTY_READ_ONLY_ERROR_MESSAGE";
	private final String cc_set_read_only_property_param_name = "CC_SET_PROPERTY_READ_ONLY_ERROR_MESSAGE";
	
	private final String printer_set_invalid_property_param_name = "PRINTER_SET_PROPERTY_INVALID_PROPERTY_VALUE_ERROR_MESSAGE";
	private final String cc_set_invalid_property_param_name = "CC_SET_PROPERTY_INVALID_PROPERTY_VALUE_ERROR_MESSAGE";
			
	private final String startDeviceJsonFileName = "startDevice.json";
	private final String stopDeviceJsonFileName = "stopDevice.json";
	private final String restartDeviceJsonFileName = "restartDevice.json";
	private final String logicalNamePlaceHolderValue = "logicalNamePlaceHolder";
	private final String categoryPlaceHolderValue = "categoryPlaceHolder";
	private final String propertyNamePlaceholderValue = "propertyNamePlaceholder";
	private final String inputJsonRelativeFilePath = "/input/json/";

	/*
	 * Step definition to call Connect API for Device.
	 */
	@When("^a user call connect API for device with input file \"([^\"]*)\"$")
	public void a_user_call_connect_API_for_device_with_input_file(String fileName) throws Throwable {
		String inputJsonFilePath = inputJsonRelativeFilePath + fileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath);
		System.out.print("\n Connect JSON Input == \n"+jsonValue +"\n");
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		// Check if session_id is null.
		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}

		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.put(getConnectDisconnectUrl());

		if (session_id == null) {
			session_id = response.getSessionId();
		}

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}

	/*
	 * Step definition to call setProperties API for Device.
	*/
	@When("a user call setProperties API for device with input file {string}")
	public void a_user_call_set_properties_api_for_device_with_input_file(String fileName) throws Throwable {
		String inputJsonFilePath = inputJsonRelativeFilePath + fileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath);
		System.out.print("\n Set Property JSON Input == \n"+jsonValue +"\n");
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		// Check if session_id is null.
		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}

		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.put(getSetPropertyUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	/*
	 * Step definition to call Disconnect API for Device.
	*/
	@When("^a user call disconnect API for device with input file \"([^\"]*)\"$")
	public void a_user_call_disconnect_API_for_device_with_input_file(String fileName) throws Throwable {
		
		String inputJsonFilePath = inputJsonRelativeFilePath + fileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath);
		System.out.print("\n Disconnect JSON Input == \n"+jsonValue +"\n");
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		// Check if session_id is null.
		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}

		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.put(getConnectDisconnectUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}

	/*
	 * Step definition to call GetProperty API.
	 */
	@When("a user call GetProperties API to get {string} property values for {string} device with terminalId {string}")
	public void a_user_call_get_properties_api_to_get_property_values_for_device_with_terminal_id(String propertyNames, String deviceName, String terminalId) throws Throwable {
		RequestSpecification httpRequest;
		
		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}
				
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		
		// Call API service.
		response = httpRequest.get(getPropertyUrl(propertyNames,deviceName,terminalId));

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	/*
	 * Step definition to call GetCapability API.
	 */
	@When("a user call GetCapabilities API to get {string} capability values for {string} device")
	public void a_user_call_get_capabilities_api_to_get_capability_values_for_device(String capabilityNames, String deviceName) throws Throwable {
		RequestSpecification httpRequest;
		
		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}
				
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		
		// Call API service.
		response = httpRequest.get(getCapabilityUrl(capabilityNames,deviceName));

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	/*
	 * Step definition to verify API response.
	 */
	@Then("^response includes the following$")
	public void response_includes_the_following(DataTable dt) throws Throwable {
		
		List<List<String>> rows = dt.asLists(String.class);

		for (List<String> columns : rows) {

			if (!columns.get(0).trim().equals("Field")) {

				if (columns.get(0).equals(message)) {
					
					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(columns.get(1)),
							response.jsonPath().get(api_response_success_message_json_path));
					
				}
				
				if (columns.get(0).equals(data)) {

					assertEquals("Response data did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_directio_data_json_path).toString());
				}
				
				if (columns.get(0).equals(additionalData)) {

					assertEquals("Response additional data did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_directio_additional_data_json_path));
				}
								
				if (columns.get(0).equals(status_code)) {

					assertEquals("Response code did not matched : ", columns.get(1),
							String.valueOf(response.getStatusCode()));
				}

				if (columns.get(0).equals(content_type)) {

					assertEquals("Content Type did not matched : ", columns.get(1), response.header(content_type));
				}

				if (columns.get(0).equals(status_line)) {

					assertEquals("Status line did not matched : ", columns.get(1), response.getStatusLine().trim());
				}

				if (columns.get(0).equals(result_code)) {

					assertEquals("resultCode did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_result_code_json_path));
				}

			}
		}

		RestAssured.reset();
		Thread.sleep(sleep);
	}

		
	/*
	 * Step definition to verify getCapability API response.
	*/
	@Then("get capability response includes the following")
	public void get_capability_response_includes_the_following(DataTable dt) throws Throwable {
		
		System.out.println("Capability Name == "+response.jsonPath().get("actions.actionBody.capabilities.name").toString().replace("[[", "").replace("]]", ""));
		System.out.println("Capability Value == "+response.jsonPath().get("actions.actionBody.capabilities.value").toString().replace(", [", "@").replace("]@", "@").replace("[[[", "").replace("]]]", ""));
		
		
		/* Extract capability names and values from response and store in string array's*/
		String [] names = response.jsonPath().get("actions.actionBody.capabilities.name").toString().replace("[[", "").replace("]]", "").split(",");
		String [] values = response.jsonPath().get("actions.actionBody.capabilities.value").toString().replace(", [", "@").replace("]@", "@").replace("[[[", "").replace("]]]", "").split("@");
		
		HashMap<String, String> capabilityNameValue = new HashMap<String, String>();
		 
		/* Store capability names and values in hash map.*/
		 for (int i=0; i< names.length;i++) {
			 capabilityNameValue.put(names[i].trim(),values[i].trim());
		 }
		 
		 List<List<String>> rows = dt.asLists(String.class);

			for (List<String> columns : rows) {

				if (!columns.get(0).trim().equals("Field")) {

					if (columns.get(0).equals(status_code)) {

						assertEquals("Response Code did not matched : ", columns.get(1), String.valueOf(response.getStatusCode()));
						
					} else if (columns.get(0).equals(content_type)) {

						assertEquals("Content Type did not matched : ", columns.get(1), response.header(content_type));
						
					} else if (columns.get(0).equals(status_line)) {

						assertEquals("Status line did not matched : ", columns.get(1), response.getStatusLine().trim());
						
					} else {

						assertEquals("Capability '"+columns.get(0)+"' value did not matched : ", columns.get(1),capabilityNameValue.get(columns.get(0)));
					}
				}
			}

			RestAssured.reset();
			Thread.sleep(sleep);
	}
	
	/*
	 * Step definition to verify getProperty API response.
	*/
	@Then("get property response includes the following")
	public void get_property_response_includes_the_following(DataTable dt)  throws Throwable {
		
		System.out.println("Property Name == "+response.jsonPath().get("actions.actionBody.properties.name").toString().replace("[[", "").replace("]]", ""));
		System.out.println("Property Value == "+response.jsonPath().get("actions.actionBody.properties.value").toString().replace(", ", "@").replace("[[", "").replace("]]", ""));
		
		/* Extract property names and values from response and store in string array's*/
		String [] names = response.jsonPath().get("actions.actionBody.properties.name").toString().replace("[[", "").replace("]]", "").split(",");
		String [] values = response.jsonPath().get("actions.actionBody.properties.value").toString().replace(", ", "@").replace("[[", "").replace("]]", "").split("@");
		
		HashMap<String, String> propertyNameValue = new HashMap<String, String>();
		 
		/* Store property names and values in hash map.*/
		 for (int i=0; i< names.length;i++) {
			 propertyNameValue.put(names[i].trim(),values[i].trim());
		 }
		 
		 List<List<String>> rows = dt.asLists(String.class);

			for (List<String> columns : rows) {

				if (!columns.get(0).trim().equals("Field")) {

					if (columns.get(0).equals(status_code)) {

						assertEquals("Response Code did not matched : ", columns.get(1), String.valueOf(response.getStatusCode()));
						
					} else if (columns.get(0).equals(content_type)) {

						assertEquals("Content Type did not matched : ", columns.get(1), response.header(content_type));
						
					} else if (columns.get(0).equals(status_line)) {

						assertEquals("Status line did not matched : ", columns.get(1), response.getStatusLine().trim());
						
					} else {

						assertEquals("Property '"+columns.get(0)+"' value did not matched : ", columns.get(1),propertyNameValue.get(columns.get(0)));
					}
				}
			}

			RestAssured.reset();
			Thread.sleep(sleep);
	}
	
	@Then("^error response includes the following$")
	public void error_response_includes_the_following(DataTable dt) throws Throwable {
		List<List<String>> rows = dt.asLists(String.class);

		for (List<String> columns : rows) {

			if (!columns.get(0).trim().equals("Field")) {

				if (columns.get(0).equals(message)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
				} else if (columns.get(0).equals(status_code)) {

					assertEquals("Response Code did not matched : ", columns.get(1),
							String.valueOf(response.getStatusCode()));
				} else if (columns.get(0).equals(content_type)) {

					assertEquals("Content Type did not matched : ", columns.get(1), response.header(content_type));
				} else if (columns.get(0).equals(status_line)) {

					assertEquals("Status line did not matched : ", columns.get(1), response.getStatusLine().trim());
				} else if (columns.get(0).equals(type)) {

					assertEquals("Error type did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_type_json_path));
				} else if (columns.get(0).equals(error_code)) {

					assertEquals("Error code did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_error_code_json_path).toString());
				} else if (columns.get(0).equals(extended_error_code)) {

					assertEquals("Extended error code did not matched : ", columns.get(1),
							response.jsonPath().get(api_response_extended_error_code_json_path).toString());
					
				} else if (columns.get(0).equals(printer_property_not_supported)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(printer_property_not_supported_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
					
				} else if (columns.get(0).equals(cc_property_not_supported)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(cc_property_not_supported_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
					
				} else if (columns.get(0).equals(printer_set_read_only_property)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(printer_set_read_only_property_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
					
				}  else if (columns.get(0).equals(rfid_set_read_only_property)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(rfid_set_read_only_property_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
					
				}   else if (columns.get(0).equals(cc_set_read_only_property)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(cc_set_read_only_property_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
					
				} else if (columns.get(0).equals(printer_set_invalid_property_value)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(printer_set_invalid_property_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
					
				} else if (columns.get(0).equals(cc_set_invalid_property_value)) {

					assertEquals("Response Message did not matched : ", Utility.getMessagePropertyValue(cc_set_invalid_property_param_name).replace(propertyNamePlaceholderValue, columns.get(1)),
							response.jsonPath().get(api_response_error_message_json_path).toString().trim());
				}
				

			}
		}

		RestAssured.reset();
		Thread.sleep(sleep);
	}
	
	/*
	 * Step definition to call start device API.
	*/
	@When("a user call start device API with device name {string} and category {string}")
	public void a_user_call_start_device_api_with_device_name_and_category(String deviceNmae, String category) throws Throwable {
		
		String inputJsonFilePath = inputJsonRelativeFilePath + startDeviceJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(logicalNamePlaceHolderValue, deviceNmae).replace(categoryPlaceHolderValue, category);
		System.out.print("\n Device Start JSON Input == \n"+jsonValue+"\n");
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();
		httpRequest = RestAssured.given();

		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.put(getDeviceAdminUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}

	/*
	 * Step definition to call stop device API.
	*/
	@When("a user call stop device API with device name {string} and category {string}")
	public void a_user_call_stop_device_api_with_device_name_and_category(String deviceNmae, String category) throws Throwable {
		
		String inputJsonFilePath = inputJsonRelativeFilePath + stopDeviceJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(logicalNamePlaceHolderValue, deviceNmae).replace(categoryPlaceHolderValue, category);
		System.out.print("\n Device Stop JSON Input == \n"+jsonValue +"\n");
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();
		httpRequest = RestAssured.given();
		
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.put(getDeviceAdminUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	/*
	 * Step definition to call restart device API.
	*/
	@When("a user call restart device API with device name {string} and category {string}")
	public void a_user_call_restart_device_api_with_device_name_and_category(String deviceNmae, String category) throws Throwable {
		
		String inputJsonFilePath = inputJsonRelativeFilePath + restartDeviceJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(logicalNamePlaceHolderValue, deviceNmae).replace(categoryPlaceHolderValue, category);
		System.out.print("\n Device Restart JSON Input == \n"+jsonValue +"\n");
		RequestSpecification httpRequest;

		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();
		httpRequest = RestAssured.given();
		
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.put(getDeviceAdminUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
	
	@DataTableType(replaceWithEmptyString = "[blank]")
	public String stringType(String cell) {
	    return cell;
	}
}