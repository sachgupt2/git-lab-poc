package com.cucumber.api.stepdefs.printer;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;
import com.cucumber.api.utils.Utility;

import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

/**
 * @author gsachin
 *
 */

 /*
 * This class contain printer device specific step definitions.
 */
public class PrinterStepDef extends BaseApiStepDef {

	
	private static Integer sleep = 300;
	private final String content_type_value = "application/json";
	private final String accept_value = "application/json";
	private final String additionalDataValuePlaceHolder = "additionalDataPlaceHolder";
	private final String setPrinterPropertyJsonFileName = "setPrinterPropertyTemplate.json";
	private final String printerDirectIoJsonFileName = "printerDirectIoTemplate.json";
	private final String dataValuePlaceHolder = "1111";
	private final String cmdNumberValuePlaceHolder = "9999";
	private final String propertyNamePlaceholderValue = "propertyNamePlaceholder";
	private final String propertyValuePlaceholder = "propertyValuePlaceholder";
	private final String inputJsonRelativeFilePath = "/input/json/";

   /*
	 * Step definition to call setProperties API for Printer Device.
	*/
	@When("a user call SetProperty API to set printer property {string} with value {string}")
	public void a_user_call_set_property_api_to_set_printer_property_with_value(String propertyName, String propertyValue) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + setPrinterPropertyJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(propertyNamePlaceholderValue, propertyName).replace(propertyValuePlaceholder, propertyValue);
		System.out.print("\n Set Property JSON Input == \n"+jsonValue +"\n");
		callSetPropertyApi(jsonValue);
	}
	
	/*
	 * Step definition to call directIO API for Printer Device.
	*/
	@When("a user call DirectIO API for printer with command number {string}, data {string} and additionaldata {string}")
	public void a_user_call_direct_io_api_for_printer_with_command_number_data_and_additionaldata(String commandNumber, String data, String additionalData) throws Throwable   {
		String inputJsonFilePath = inputJsonRelativeFilePath + printerDirectIoJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(cmdNumberValuePlaceHolder, commandNumber).replace(dataValuePlaceHolder, data).replace(additionalDataValuePlaceHolder, additionalData);
		System.out.print("\n DirectIO JSON Input == \n"+jsonValue +"\n");
		callDirectIoApi(jsonValue);
	}

	/*
	 * Step definition to call Print API for Printer Device.
	*/
	@When("^a user call Print API for device with input file \"([^\"]*)\"$")
	public void a_user_call_Print_API_for_device_with_input_file(String fileName) throws Throwable {
		String inputJsonFilePath = inputJsonRelativeFilePath + fileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath);
		System.out.print("\n Print JSON Input == \n"+jsonValue +"\n");
		RequestSpecification httpRequest;
		
		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();
		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}
			
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		httpRequest.body(jsonValue);

		// Call API service.
		response = httpRequest.post(getPrintReceiptUrl());

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
}