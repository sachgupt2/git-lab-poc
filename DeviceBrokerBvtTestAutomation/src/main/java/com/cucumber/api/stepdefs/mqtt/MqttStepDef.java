package com.cucumber.api.stepdefs.mqtt;

import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.awaitility.Awaitility;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.skyscreamer.jsonassert.JSONAssert;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.Given;

/**
 * @author gsachin
 *
 */
public class MqttStepDef extends BaseApiStepDef {

	private String messageAddress;
	private MqttClient client;
	private String clientId;
	private String messageResponse;
	private boolean isMessageEvent = false;

	/**
	 * Constructor
	 * 
	 * @throws Throwable
	 */
	public MqttStepDef() throws Throwable {
		messageAddress = getMqttUrl();
	}

	@Given("^Connect to message broker$")
	public void connect_to_message_broker() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		this.clientId = getMqttClientId();

		client = new MqttClient(messageAddress, clientId);
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		client.connect(connOpts);
		client.setCallback(callback);
	}

	@Then("^Subscribe topic \"([^\"]*)\"$")
	public void subscribe_topic(String topic) throws Throwable {
		System.out.println("Inside : subscribe_topic()");
		client.subscribe(topic, 2);
	}

	@Then("^Request human action: \"([^\"]*)\"$")
	public void request_human_action(String message) throws Throwable {
		System.out.println("Inside : request_human_action()");
		JOptionPane.showMessageDialog(null, message, "Notification", 1);
	}

	@Then("^In (\\d+) seconds should receive message$")
	public void in_seconds_should_receive_message(int time, String data) throws Throwable {
		System.out.println("COMPARE MESSAGE FUNCTION");
		Awaitility.await().atMost(time, TimeUnit.SECONDS).until(() -> isMessageEvent == true);
		isMessageEvent = false;
		JSONAssert.assertEquals(data, messageResponse, false);
	}

	@Then("^Unsubscribe topic \"([^\"]*)\"$")
	public void unsubscribe_topic(String topic) throws Throwable {
		System.out.println("Inside : unsubscribe_topic()");
		client.unsubscribe(topic);
	}

	@Then("^Disconnect Broker$")
	public void disconnect_Broker() throws Throwable {
		System.out.println("Inside : disconnect_Broker()");
		client.disconnect();
	}

	private MqttCallback callback = new MqttCallback() {
		/**
		 * This method is called when the connection to the server is lost.
		 */
		public void connectionLost(Throwable t) {
			System.out.println("LOST CONNECTION");
			boolean isSuccess = false;
			while (isSuccess == false) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				// try to connect when connection lost
				try {
					MqttStepDef.this.connect_to_message_broker();
					System.out.println("Connect again success");
					isSuccess = true;
					break;
				} catch (Exception e) {
					System.out.println("Connect again fail");
					isSuccess = false;
					e.printStackTrace();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		/**
		 * This method is called when a message arrives from the server.
		 */
		public void messageArrived(String topic, MqttMessage message) throws Exception {
			System.out.println("\n --------------- MESSAGE ARRIVED:-------------------------------- \n" + message.toString() + "\n ---------------------------------------------------------");
			isMessageEvent = true;
			messageResponse = message.toString();
		}

		/**
		 * Called when delivery for a message has been completed, and all
		 * acknowledgments have been received
		 */
		public void deliveryComplete(IMqttDeliveryToken arg0) {
			System.out.println("DELIVERY COMPLETE");
		}
	};
}