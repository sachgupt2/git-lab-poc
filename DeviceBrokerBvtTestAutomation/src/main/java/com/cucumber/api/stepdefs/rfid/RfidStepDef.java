package com.cucumber.api.stepdefs.rfid;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;
import com.cucumber.api.utils.Utility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author gsachin
 *
 */

/*
 * This class contain RFIDScanner step definitions.
 */
public class RfidStepDef extends BaseApiStepDef {

	private final String additionalDataValuePlaceHolder = "additionalDataPlaceHolder";
	private final String setRfidScannerPropertyJsonFileName = "setRfidScannerPropertyTemplate.json";
	private final String rfidScannerDirectIoJsonFileName = "rfidScannerDirectIoTemplate.json";
	private final String readTagJsonFileName = "readTagTemplate.json";
	private final String stopReadTagJsonFileName = "stopReadTagTemplate.json";
	private final String writeTagIdJsonFileName = "writeTagIdTemplate.json";
	
	private final String dataValuePlaceHolder = "1111";
	private final String cmdNumberValuePlaceHolder = "9999";
	private final String propertyNamePlaceholderValue = "propertyNamePlaceholder";
	private final String propertyValuePlaceholder = "propertyValuePlaceholder";
	private final String readTagOperation = "readTag";
	private final String startReadTagOperation = "startReadTag";
	private final String stopReadTagOperation = "stopReadTag";
	private final String writeTagIdOperation = "writeTagId";
	
	private final String inputJsonRelativeFilePath = "/input/json/";

	
	/*
	 * Step definition to call setProperties API for RFIDScanner Device.
	*/
	@When("a user call SetProperty API to set RFIDScanner property {string} with value {string}")
	public void a_user_call_set_property_api_to_set_rfid_scanner_property_with_value(String propertyName, String propertyValue) throws Throwable  {
	    
		String inputJsonFilePath = inputJsonRelativeFilePath + setRfidScannerPropertyJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(propertyNamePlaceholderValue, propertyName).replace(propertyValuePlaceholder, propertyValue);
		System.out.print("\n Set Property JSON Input == \n"+jsonValue +"\n");
		callSetPropertyApi(jsonValue);
	}
	
	/*
	 * Step definition to call directIO API for RFIDScanner Device.
	*/
	@When("a user call DirectIO API for RFIDScanner with command number {string}, data {string} and additionaldata {string}")
	public void a_user_call_direct_io_api_for_rfid_scanner_with_command_number_data_and_additionaldata(String commandNumber, String data, String additionalData) throws Throwable   {
		String inputJsonFilePath = inputJsonRelativeFilePath + rfidScannerDirectIoJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(cmdNumberValuePlaceHolder, commandNumber).replace(dataValuePlaceHolder, data).replace(additionalDataValuePlaceHolder, additionalData);
		System.out.print("\n DirectIO JSON Input == \n"+jsonValue +"\n");
		callDirectIoApi(jsonValue);
	}

	/*
	 * Step definition to call read tag API.
	*/
	@When("a user call read tag API with following values in table")
	public void a_user_call_read_tag_api_with_following_values_in_table(DataTable dt) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + readTagJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n Read Tag JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getRfidApiUrl());
	}
	
	/*
	 * Step definition to call start read tag API.
	*/
	@When("a user call start read tag API with following values in table")
	public void a_user_call_start_read_tag_api_with_following_values_in_table(DataTable dt) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + readTagJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n Start Read Tag JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getRfidApiUrl());
		Thread.sleep(1000);
	}
	
	/*
	 * Step definition to call stop read tag API.
	*/
	@When("a user call stop read tag API with following values in table")
	public void a_user_call_stop_read_tag_api_with_following_values_in_table(DataTable dt) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + stopReadTagJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n Stop Read Tag JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getRfidApiUrl());
	}
	
	/*
	 * Step definition to call write tag API.
	*/
	@When("a user call write tag API with following values in table")
	public void a_user_call_write_tag_api_with_following_values_in_table(DataTable dt) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + writeTagIdJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n Read Tag JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getRfidApiUrl());
	}

	/*
	 * Step definition to verify read tag API response.
	*/
	@Then("read tag api response includes the following")
	public void read_tag_api_response_includes_the_following(DataTable dt) throws Throwable  {
		verifyDeviceSpecificApiResponse(readTagOperation, dt);
	}
	
	/*
	 * Step definition to verify start read tag API response.
	*/
	@Then("start read tag api response includes the following")
	public void start_read_tag_api_response_includes_the_following(DataTable dt) throws Throwable  {
		verifyDeviceSpecificApiResponse(startReadTagOperation, dt);
	}
	
	/*
	 * Step definition to verify read tag API response.
	*/
	@Then("stop read tag api response includes the following")
	public void stop_read_tag_api_response_includes_the_following(DataTable dt) throws Throwable  {
		verifyDeviceSpecificApiResponse(stopReadTagOperation, dt);
	}
	
	/*
	 * Step definition to verify write tag API response.
	*/
	@Then("write tag api response includes the following")
	public void write_tag_api_response_includes_the_following(DataTable dt) throws Throwable  {
		verifyDeviceSpecificApiResponse(writeTagIdOperation, dt);
	}
}