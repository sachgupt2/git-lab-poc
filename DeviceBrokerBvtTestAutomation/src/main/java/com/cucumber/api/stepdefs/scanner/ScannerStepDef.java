package com.cucumber.api.stepdefs.scanner;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;
import com.cucumber.api.utils.Utility;

import io.cucumber.java.en.When;

/**
 * @author gsachin
 *
 */

/*
 * This class contain Scanner step definitions.
 */
public class ScannerStepDef extends BaseApiStepDef {

	private final String additionalDataValuePlaceHolder = "additionalDataPlaceHolder";
	private final String scannerDirectIoJsonFileName = "scannerDirectIoTemplate.json";
	private final String setScannerPropertyJsonFileName = "setScannerPropertyTemplate.json";
	private final String dataValuePlaceHolder = "1111";
	private final String cmdNumberValuePlaceHolder = "9999";
	private final String propertyNamePlaceholderValue = "propertyNamePlaceholder";
	private final String propertyValuePlaceholder = "propertyValuePlaceholder";
	private final String inputJsonRelativeFilePath = "/input/json/";

	/*
	 * Step definition to call setProperties API for Scanner Device.
	*/
	@When("a user call SetProperty API to set Scanner property {string} with value {string}")
	public void a_user_call_set_property_api_to_set_scanner_property_with_value(String propertyName, String propertyValue) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + setScannerPropertyJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(propertyNamePlaceholderValue, propertyName).replace(propertyValuePlaceholder, propertyValue);
		System.out.print("\n Set Property JSON Input == \n"+jsonValue +"\n");
		callSetPropertyApi(jsonValue);
	}
	
	/*
	 * Step definition to call directIO API for Scanner Device.
	*/
	@When("a user call DirectIO API for Scanner with command number {string}, data {string} and additionaldata {string}")
	public void a_user_call_direct_io_api_for_scanner_with_command_number_data_and_additionaldata(String commandNumber, String data, String additionalData) throws Throwable   {
		String inputJsonFilePath = inputJsonRelativeFilePath + scannerDirectIoJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(cmdNumberValuePlaceHolder, commandNumber).replace(dataValuePlaceHolder, data).replace(additionalDataValuePlaceHolder, additionalData);
		System.out.print("\n DirectIO JSON Input == \n"+jsonValue +"\n");
		callDirectIoApi(jsonValue);
	}
}