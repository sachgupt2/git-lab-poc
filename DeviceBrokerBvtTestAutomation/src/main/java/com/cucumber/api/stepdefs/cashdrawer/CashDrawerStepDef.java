package com.cucumber.api.stepdefs.cashdrawer;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;
import com.cucumber.api.utils.Utility;

import io.cucumber.java.en.When;

/**
 * @author gsachin
 *
 */
/*
 * This class contain cash drawer step definitions.
 */
public class CashDrawerStepDef extends BaseApiStepDef {

	private final String setCashDrawerPropertyJsonFileName = "setCashDrawerPropertyTemplate.json";
	private final String propertyNamePlaceholderValue = "propertyNamePlaceholder";
	private final String propertyValuePlaceholder = "propertyValuePlaceholder";
	private final String inputJsonRelativeFilePath = "/input/json/";
	
	/*
	 * Step definition to call setProperties API for Cash Drawer Device.
	*/
	@When("a user call SetProperty API to set CashDrawer property {string} with value {string}")
	public void a_user_call_set_property_api_to_set_cash_drawer_property_with_value(String propertyName, String propertyValue) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + setCashDrawerPropertyJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(propertyNamePlaceholderValue, propertyName).replace(propertyValuePlaceholder, propertyValue);
		System.out.print("\n Set Property JSON Input == \n"+jsonValue +"\n");
		callSetPropertyApi(jsonValue);
	}
}