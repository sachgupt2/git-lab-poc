package com.cucumber.api.stepdefs.cashchanger;

import com.cucumber.api.stepdefs.base.BaseApiStepDef;
import com.cucumber.api.utils.Utility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

/**
 * @author gsachin
 *
 */
/*
 * This class contain cash changer step definitions.
 */
public class CashChangerStepDef extends BaseApiStepDef {

	private static String session_id = null;
	private static Integer sleep = 300;
	private final String content_type_value = "application/json";
	private final String accept_value = "application/json";
	private final String additionalDataValuePlaceHolder = "additionalDataPlaceHolder";
	private final String setCashChangerPropertyJsonFileName = "setCashChangerPropertyTemplate.json";
	private final String cashChangerDirectIoJsonFileName = "cashChangerDirectIoTemplate.json";
	private final String beginDepositJsonFileName = "beginDepositTemplate.json";
	private final String endDepositJsonFileName = "endDepositTemplate.json";
	private final String dispenseCashJsonFileName = "dispenseCashTemplate.json";
	private final String dataValuePlaceHolder = "1111";
	private final String cmdNumberValuePlaceHolder = "9999";
	private final String propertyNamePlaceholderValue = "propertyNamePlaceholder";
	private final String propertyValuePlaceholder = "propertyValuePlaceholder";
	private final String beginDepositOperation = "beginDeposit";
	private final String endDepositOperation = "endDeposit";
	
	
	private final String inputJsonRelativeFilePath = "/input/json/";

	/*
	 * Step definition to call setProperties API for Cash Changer Device.
	*/
	@When("a user call SetProperty API to set cash changer property {string} with value {string}")
	public void a_user_call_set_property_api_to_set_cash_changer_property_with_value(String propertyName, String propertyValue) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + setCashChangerPropertyJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(propertyNamePlaceholderValue, propertyName).replace(propertyValuePlaceholder, propertyValue);
		System.out.print("\n Set Property JSON Input == \n"+jsonValue +"\n");
		callSetPropertyApi(jsonValue);
	}
	
	/*
	 * Step definition to call directIO API for Cash Changer Device.
	*/
	@When("a user call DirectIO API for cash changer with command number {string}, data {string} and additionaldata {string}")
	public void a_user_call_direct_io_api_for_cash_changer_with_command_number_data_and_additionaldata(String commandNumber, String data, String additionalData) throws Throwable   {
		String inputJsonFilePath = inputJsonRelativeFilePath + cashChangerDirectIoJsonFileName;
		String jsonValue = Utility.generateStringFromResource(inputJsonFilePath).replace(cmdNumberValuePlaceHolder, commandNumber).replace(dataValuePlaceHolder, data).replace(additionalDataValuePlaceHolder, additionalData);
		System.out.print("\n DirectIO JSON Input == \n"+jsonValue +"\n");
		callDirectIoApi(jsonValue);
	}
		
	/*
	 * Step definition to verify begin deposit API response.
	*/
	@Then("begin deposit api response includes the following")
	public void begin_deposit_api_response_includes_the_following(DataTable dt) throws Throwable {
		verifyDeviceSpecificApiResponse(beginDepositOperation, dt);
	}

	/*
	 * Step definition to verify end deposit API response.
	*/
	@Then("end deposit api response includes the following")
	public void end_deposit_api_response_includes_the_following(DataTable dt) throws Throwable {
		verifyDeviceSpecificApiResponse(endDepositOperation, dt);
	}

	/*
	 * Step definition to call begin deposit API.
	*/
	@When("a user call begin deposit API with input file {string}")
	public void a_user_call_begin_deposit_api_with_input_file(String fileName) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + fileName;
		String inputJson = Utility.generateStringFromResource(inputJsonFilePath);
		System.out.print("\n Begin Deposit JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getBeginEndDepositUrl());
	}
	
	/*
	 * Step definition to call begin deposit API.
	*/
	@When("a user call begin deposit API with following values in table")
	public void a_user_call_begin_cash_deposit_api_with_following_values_in_table(DataTable dt) throws Throwable {
		String inputJsonFilePath = inputJsonRelativeFilePath + beginDepositJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n Begin Deposit JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getBeginEndDepositUrl());
	}
	
	/*
	 * Step definition to call end deposit API.
	*/
	@When("a user call end deposit API with following values in table")
	public void a_user_call_end_cash_deposit_api_with_following_values_in_table(DataTable dt) throws Throwable {
		String inputJsonFilePath = inputJsonRelativeFilePath + endDepositJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n End Deposit JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getBeginEndDepositUrl());
	}
	
	/*
	 * Step definition to call dispense cash API.
	*/
	@When("a user call dispense cash API with following values in table")
	public void a_user_call_dispense_cash_api_with_following_values_in_table(DataTable dt) throws Throwable  {
		String inputJsonFilePath = inputJsonRelativeFilePath + dispenseCashJsonFileName;
		String jsonTemplateValue = Utility.generateStringFromResource(inputJsonFilePath);
		String inputJson = prepareInputJsonRequest(jsonTemplateValue,dt);
		System.out.print("\n Dispense Cash JSON Input == \n"+inputJson +"\n");
		callPostApi(inputJson,getDispenseCashUrl());
	}
	
	/*
	 * Step definition to call Read cash count for Cash Changer Device.
	*/
	@When("a user call read cash count API with terminalId {string}")
	public void a_user_call_read_cash_count_api_with_terminal_id(String terminalId) throws Throwable  {
		RequestSpecification httpRequest;
		
		// Set base url for API.
		RestAssured.baseURI = getBaseUrl();

		if (session_id == null) {
			httpRequest = RestAssured.given();
		} else {
			httpRequest = RestAssured.given().sessionId(session_id);
		}
				
		// Set content type for request.
		httpRequest.contentType(content_type_value);
		httpRequest.accept(accept_value);
		
		// Call API service.
		response = httpRequest.get(getReadCashCountUrl(terminalId));

		System.out.println("\n");
		System.out.println(response.prettyPeek());
		Thread.sleep(sleep);
	}
		
	
}