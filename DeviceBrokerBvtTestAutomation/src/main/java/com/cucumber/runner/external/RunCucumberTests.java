package com.cucumber.runner.external;

import com.cucumber.api.utils.Utility;

/*
 * Cucumber Runner class to execute test scripts using batch file. 
 */
public class RunCucumberTests {

	public static void main(String[] args) throws Exception {

		Utility.setFilePath("fromJarFile");
		io.cucumber.core.cli.Main.main(args);
	}

}
