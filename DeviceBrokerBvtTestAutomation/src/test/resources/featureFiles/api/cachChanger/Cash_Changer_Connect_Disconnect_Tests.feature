Feature: Cash Changer Connect Disconnect Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @Connect
  Scenario: User calls Connect, and disconnect API for Cash Changer device and verify response.
            This scenario covers test cases DB-CAP-CC-ConDiscon-1, DB-CAP-CC-ConDiscon-2

    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
      
  @CC @BVT @REG @Connect
  Scenario: User calls Connect API 2 times for Cash Changer device and verify response.
            This scenario covers test cases DB-CAP-CC-ConDiscon-5

    When a user call connect API for device with input file "ConnectCashChanger.json"
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then error response includes the following
      | Field                | Value                                      |
      | Status Code          | 409                                        |
      | Content-Type         | application/json                           |
      | Status line          | HTTP/1.1 409                               |
      | type                 | Minor                                      |
      | errorCode            | 1115                                       |
      | extendedErrorCode    | 0                                          |
      | message              | CC_DUPLICATE_CONNECT_REQUEST_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
      
  @CC @BVT @REG @Connect
  Scenario: User calls disconnect API for Cash Changer device without calling connect API and verify response.
            This scenario covers test cases DB-CAP-CC-ConDiscon-6

    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then error response includes the following
      | Field                | Value                                      |
      | Status Code          | 400                                        |
      | Content-Type         | application/json                           |
      | Status line          | HTTP/1.1 400                               |
      | type                 | Major                                      |
      | errorCode            | 1305                                       |
      | extendedErrorCode    | 0                                          |
      | message              | CC_ALREADY_DISCONNECTED_ERROR_MESSAGE      |
  
  @CC @BVT @REG @Connect
  Scenario: User calls Connect API 2 times with different terminal id and verify response.
            This scenario covers test cases DB-CAP-CC-ConDiscon-10
    When a user call connect API for device with input file "ConnectCashChanger.json"
    When a user call connect API for device with input file "ConnectCashChangerDifferentTerminalId.json"
    Then error response includes the following
      | Field                | Value                                                  |
      | Status Code          | 409                                                    |
      | Content-Type         | application/json                                       |
      | Status line          | HTTP/1.1 409                                           |
      | type                 | Minor                                                  |
      | errorCode            | 1101                                                   |
      | extendedErrorCode    | 0                                                      |
      | message              | CC_DIFFERENT_TERMINAL_ID_CONNECT_REQUEST_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
  @CC @BVT @REG @Connect
  Scenario: User calls Connect API with invalid logical name and verify response.
            This scenario covers test cases DB-CAP-CC-ConDiscon-11
    When a user call connect API for device with input file "ConnectCashChangerInvalidLogicalName.json"
    Then error response includes the following
      | Field                | Value                                    |
      | Status Code          | 400                                      |
      | Content-Type         | application/json                         |
      | Status line          | HTTP/1.1 400                             |
      | type                 | Major                                    |
      | errorCode            | 1105                                     |
      | extendedErrorCode    | 0                                        |
      | message              | CC_DEVICE_NOT_FOUND_ERROR_MESSAGE        |