Feature: Cash Changer Dispense Cash Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-43
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 50:20,100:20,1000:-2,10000:1  |
      
    Then error response includes the following
      | Field                | Value                                                         |
      | Status Code          | 400                                                           |
      | Content-Type         | application/json                                              |
      | Status line          | HTTP/1.1 400                                                  |
      | type                 | Minor                                                         |
      | errorCode            | 1207                                                          |
      | extendedErrorCode    | 0                                                             |
      | message              | CC_DISPENSE_CASH_WITH_INVALID_CASH_COUNTS_VALUE_ERROR_MESSAGE |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-44
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 50:20,100:-20,1000:2,10000:1  |
      
    Then error response includes the following
      | Field                | Value                                                         |
      | Status Code          | 400                                                           |
      | Content-Type         | application/json                                              |
      | Status line          | HTTP/1.1 400                                                  |
      | type                 | Minor                                                         |
      | errorCode            | 1207                                                          |
      | extendedErrorCode    | 0                                                             |
      | message              | CC_DISPENSE_CASH_WITH_INVALID_CASH_COUNTS_VALUE_ERROR_MESSAGE |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-45
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 50:20,100:20,1000:0,10000:1   |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-46
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 50:0,100:20,1000:2,10000:1    |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-59
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 10:11,100:3,500:4             |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-60
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 10:10,100:11,500:4            |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-61
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 10:10,100:10,500:6            |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-62
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 5:11,100:3,500:4              |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-63
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 50:11,100:3,500:4             |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-64
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 1:11,100:3,500:4              |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-66
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 1000:301,10000:1              |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-67
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 1000:2,10000:101              |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-68
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 5000:101,10000:1              |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-70
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                                          |
      | terminalID                          | prime55                                        |
      | category                            | CashChanger                                    |
      | logicalName                         | CashChanger                                    |
      | operationType                       | DispenseCash                                   |
      | cashCounts                          | 5:11,10:10,100:10,500:4,1000:2,5000:1,10000:1  |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-71
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call dispense cash API with following values in table
      | Field                               | Value                                            |
      | terminalID                          | prime55                                          |
      | category                            | CashChanger                                      |
      | logicalName                         | CashChanger                                      |
      | operationType                       | DispenseCash                                     |
      | cashCounts                          | 5:10,10:10,100:10,500:4,1000:301,5000:1,10000:1  |
      
    Then error response includes the following
      | Field                | Value                             |
      | Status Code          | 500                               |
      | Content-Type         | application/json                  |
      | Status line          | HTTP/1.1 500                      |
      | type                 | Major                             |
      | errorCode            | 106                               |
      | extendedErrorCode    | 0                                 |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
    @CC @BVT @REG @DispenseCash
  Scenario: User calls dispense cash API after calling begin deposit API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-19
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | BeginDeposit             |
      
    Then begin deposit api response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_BEGIN_DEPOSIT_REQUEST_MESSAGE |
    
    When a user call dispense cash API with following values in table
      | Field                               | Value                         |
      | terminalID                          | prime55                       |
      | category                            | CashChanger                   |
      | logicalName                         | CashChanger                   |
      | operationType                       | DispenseCash                  |
      | cashCounts                          | 1:3,10:2,100:3                |
      
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 500                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 500                                         |
      | type                 | Major                                                |
      | errorCode            | 1202                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | CC_DISPENSE_CASH_AFTER_BEGIN_DEPOSIT_ERROR_MESSAGE   |
      
    When a user call end deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | EndDeposit               |
      | state                               | 1                        |
    
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_END_DEPOSIT_REQUEST_MESSAGE      |
          
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then end deposit api response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |