Feature: Cash Changer Capability Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API with invalid capability name 'FAX' and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-3
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "FAX" capability values for "CashChanger" device
	  Then error response includes the following
      | Field                | Value                                                     |
      | Status Code          | 400                                                       |
      | Content-Type         | application/json                                          |
      | Status line          | HTTP/1.1 400                                              |
      | type                 | Minor                                                     |
      | errorCode            | 1204                                                      |
      | extendedErrorCode    | 0                                                         |
      | message              | CC_GET_CAPABILITY_WITH_INVALID_CAPABILITY_NAME_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-5
    When a user call GetCapabilities API to get "all" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapConnection       | true                                                      |
     | CapDiscrepancy      | true                                                      |
     | CapSensor           | DB_CS_EMPTY,DB_CS_FULL,DB_CS_NEAR_EMPTY,DB_CS_NEAR_FULL   |
     | CapPauseDeposit     | true                                                      |
     | CapRepayDeposit     | false                                                     |
     | CapRealTimeData     | false                                                     |
     
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-6
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "all" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapConnection       | true                                                      |
     | CapDiscrepancy      | true                                                      |
     | CapSensor           | DB_CS_EMPTY,DB_CS_FULL,DB_CS_NEAR_EMPTY,DB_CS_NEAR_FULL   |
     | CapPauseDeposit     | true                                                      |
     | CapRepayDeposit     | false                                                     |
     | CapRealTimeData     | false                                                     |
    
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-10
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapConnection" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapConnection       | true                                                      |
     
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-15
    When a user call GetCapabilities API to get "CapDiscrepancy" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapDiscrepancy      | true                                                      |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-16
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapDiscrepancy" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapDiscrepancy      | true                                                      |
     
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-21
    When a user call GetCapabilities API to get "CapSensor" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapSensor           | DB_CS_EMPTY,DB_CS_FULL,DB_CS_NEAR_EMPTY,DB_CS_NEAR_FULL   |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-22
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapSensor" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapSensor           | DB_CS_EMPTY,DB_CS_FULL,DB_CS_NEAR_EMPTY,DB_CS_NEAR_FULL   |
     
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |

  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-27
    When a user call GetCapabilities API to get "CapPauseDeposit" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapPauseDeposit     | true                                                      |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-28
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapPauseDeposit" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapPauseDeposit     | true                                                      |
     
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-33
    When a user call GetCapabilities API to get "CapRepayDeposit" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapRepayDeposit     | false                                                     |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-34
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapRepayDeposit" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapRepayDeposit     | false                                                     |
     
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-39
    When a user call GetCapabilities API to get "CapRealTimeData" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapRealTimeData     | false                                                     |
  
  @CC @BVT @REG @Capability
  Scenario: User calls getCapabilities API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetCap-40
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapRealTimeData" capability values for "CashChanger" device
	  Then get capability response includes the following
     | Field               | Value                                                     |
     | Status Code         | 200                                                       |
     | Content-Type        | application/json                                          |
     | Status line         | HTTP/1.1 200                                              |
     | CapRealTimeData     | false                                                     |
     
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |