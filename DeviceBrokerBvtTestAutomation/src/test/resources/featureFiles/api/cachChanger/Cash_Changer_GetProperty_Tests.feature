Feature: Cash Changer Get Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-1
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "all" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                                   |
      | Status Code                 | 200                                     |
      | Content-Type                | application/json                        |
      | Status line                 | HTTP/1.1 200                            |
      | CurrencyCode                | JPY                                     |
	    | ExitCashList                | 1,5,10,50,100,500;1000,5000,10000       |
	    | CurrencyCashList            | 1,5,10,50,100,500;1000,2000,5000,10000  |
      | DepositCodeList             | JPY                                     |
      | DepositCashList             | 1,5,10,50,100,500;1000,2000,5000,10000  |
      | AsyncMode                   | false                                   |
      | DeviceExits                 | 1                                       |
      | CurrencyCodeList            | JPY                                     |
      | DeviceStatus                | 12                                      |
      | ConnectionStatus            | Connected                               |
      | CurrentExit                 | 1                                       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-4
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "FAX" property values for "CashChanger" device with terminalId "prime55"
    Then error response includes the following
      | Field                | Value                                                     |
      | Status Code          | 400                                                       |
      | Content-Type         | application/json                                          |
      | Status line          | HTTP/1.1 400                                              |
      | type                 | Minor                                                     |
      | errorCode            | 1204                                                      |
      | extendedErrorCode    | 0                                                         |
      | message              | CC_GET_CAPABILITY_WITH_INVALID_PROPERTY_NAME_MESSAGE      |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |  
  
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-6
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "ConnectionStatus" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                                   |
      | Status Code                 | 200                                     |
      | Content-Type                | application/json                        |
      | Status line                 | HTTP/1.1 200                            |
      | ConnectionStatus            | Connected                               |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-7
    When a user call GetProperties API to get "all" property values for "CashChanger" device with terminalId "prime55"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | CC_GET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-8
    When a user call GetProperties API to get "ConnectionStatus" property values for "CashChanger" device with terminalId "prime55"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | CC_GET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE       |
  
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-10
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
    
    When a user call SetProperty API to set cash changer property "AsyncMode" with value "true"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | CC_SET_PROPERTY_REQUEST_MESSAGE       |
        
    When a user call GetProperties API to get "AsyncMode" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | AsyncMode               | true                                    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-11
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "AsyncMode" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | AsyncMode               | false                                   |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-14
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "CurrencyCashList" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | CurrencyCashList        | 1,5,10,50,100,500;1000,2000,5000,10000  |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-19
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "CurrencyCode" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | CurrencyCode            | JPY                                     |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-25
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "CurrentExit" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | CurrentExit             | 1                                       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-28
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "DepositCashList" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | DepositCashList         | 1,5,10,50,100,500;1000,2000,5000,10000  |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-31
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "DepositCodeList" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | DepositCodeList         | JPY                                     |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-34
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "DeviceStatus" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | DeviceStatus            | 12                                      |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
  
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-37
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "DeviceExits" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | DeviceExits             | 1                                       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
          
  @CC @BVT @REG @Property @GetProperty
  Scenario: User calls getProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-GetProp-40
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "ExitCashList" property values for "CashChanger" device with terminalId "prime55"
    Then get property response includes the following
      | Field                   | Value                                   |
      | Status Code             | 200                                     |
      | Content-Type            | application/json                        |
      | Status line             | HTTP/1.1 200                            |
      | ExitCashList            | 1,5,10,50,100,500;1000,5000,10000       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |