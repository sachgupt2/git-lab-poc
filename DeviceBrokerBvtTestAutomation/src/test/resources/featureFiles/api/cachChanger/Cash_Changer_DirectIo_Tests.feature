Feature: Cash Changer DirectIo Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @REG @DirectIo
  Scenario: User calls DirectIo API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-71
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call DirectIO API for cash changer with command number "3", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value                                                       |
      | Status Code                 | 200                                                         |
      | Content-Type                | application/json                                            |
      | Status line                 | HTTP/1.1 200                                                |
      | data                        | -2147483648                                                 |
      | additionalData              | 1:0,5:0,10:0,50:0,100:0,500:0;1000:0,2000:0,5000:0,10000:0  |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |