Feature: Cash Changer Begin End Deposit Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @REG @BeginDeposit
  Scenario: User calls begin deposit API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-1
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | BeginDeposit             |
      
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | CC_BEGIN_DEPOSIT_WITH_OUT_CONNECT_ERROR_MESSAGE      |
      
  @CC @BVT @REG @BeginDeposit
  Scenario: User calls begin deposit API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-2
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | BeginDeposit             |
      
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_BEGIN_DEPOSIT_REQUEST_MESSAGE |
      
   When a user call end deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | EndDeposit               |
      | state                               | 1                        |
    
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_END_DEPOSIT_REQUEST_MESSAGE      |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @BeginDeposit
  Scenario: User calls begin deposit API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-3
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | BeginDeposit             |
      
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime66                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | BeginDeposit             |
      
    Then error response includes the following
      | Field                | Value                                                  |
      | Status Code          | 409                                                    |
      | Content-Type         | application/json                                       |
      | Status line          | HTTP/1.1 409                                           |
      | type                 | Minor                                                  |
      | errorCode            | 1104                                                   |
      | extendedErrorCode    | 0                                                      |
      | message              | CC_DIFFERENT_TERMINAL_ID_BEGIN_DEPOSIT_REQUEST_MESSAGE |
      
    When a user call end deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | EndDeposit               |
      | state                               | 1                        |
    
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_END_DEPOSIT_REQUEST_MESSAGE      |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @BeginDeposit
  Scenario: User calls begin deposit API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-6
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger1             |
      | operationType                       | BeginDeposit             |
      
    Then error response includes the following
      | Field                | Value                                                  |
      | Status Code          | 400                                                    |
      | Content-Type         | application/json                                       |
      | Status line          | HTTP/1.1 400                                           |
      | type                 | Minor                                                  |
      | errorCode            | 1207                                                   |
      | extendedErrorCode    | 0                                                      |
      | message              | CC_BEGIN_DEPOSIT_INVALID_LOGICAL_NAME_ERROR_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @BeginDeposit
  Scenario: User calls begin deposit API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-7
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call begin deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | [blank]                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | BeginDeposit             |
      
    Then error response includes the following
      | Field                | Value                                                  |
      | Status Code          | 400                                                    |
      | Content-Type         | application/json                                       |
      | Status line          | HTTP/1.1 400                                           |
      | type                 | Minor                                                  |
      | errorCode            | 1207                                                   |
      | extendedErrorCode    | 0                                                      |
      | message              | CC_BEGIN_DEPOSIT_WITH_OUT_TERMINAL_ID_ERROR_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @BeginDeposit
  Scenario: User calls begin deposit API wit invalid JSON when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-12
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call begin deposit API with input file "BeginCashDepositWithInvalidJsonRequest.json"
    Then error response includes the following
      | Field                | Value                            |
      | Status Code          | 400                              |
      | Content-Type         | application/json                 |
      | Status line          | HTTP/1.1 400                     |
      | type                 | Minor                            |
      | errorCode            | 1207                             |
      | extendedErrorCode    | 0                                |
      | message              | CC_INVALID_JSON_ERROR_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @EndDeposit
  Scenario: User calls end deposit API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-Regression-16
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call end deposit API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | CashChanger              |
      | logicalName                         | CashChanger              |
      | operationType                       | EndDeposit               |
      | state                               | 1                        |
      
    Then error response includes the following
      | Field                | Value                                                  |
      | Status Code          | 500                                                    |
      | Content-Type         | application/json                                       |
      | Status line          | HTTP/1.1 500                                           |
      | type                 | Major                                                  |
      | errorCode            | 1202                                                   |
      | extendedErrorCode    | 0                                                      |
      | message              | CC_END_DEPOSIT_WITH_OUT_BEGIN_DEPOSIT_ERROR_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |