Feature: Cash Changer Set Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashChanger.json"
  	
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'IDLE' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-1
    When a user call SetProperty API to set cash changer property "ConnectionStatus" with value "false"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | CC_SET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE       |
  
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-2
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "ConnectionStatus" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | cc-set-read-only-property       | ConnectionStatus         |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-5
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "WrongConnectionStatus" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1204                     |
      | extendedErrorCode               | 0                        |
      | CashChangerPropertyNotSupported | WrongConnectionStatus    |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-17
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "CurrencyCode" with value "JPY"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | CC_SET_PROPERTY_REQUEST_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-18
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "CurrencyCode" with value "USD"
    Then error response includes the following
      | Field                               | Value                    |
      | Status Code                         | 400                      |
      | Content-Type                        | application/json         |
      | Status line                         | HTTP/1.1 400             |
      | type                                | Minor                    |
      | errorCode                           | 1204                     |
      | extendedErrorCode                   | 0                        |
      | cc-set-invalid-property-value       | CurrencyCode             |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-19
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "AsyncMode" with value "true"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | CC_SET_PROPERTY_REQUEST_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-20
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "AsyncMode" with value "false"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | CC_SET_PROPERTY_REQUEST_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-21
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "CurrentExit" with value "1"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | CC_SET_PROPERTY_REQUEST_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |
      
  @CC @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when device is in 'BUSY' state and verify response.
            This scenario covers test cases DB-CAP-CC-SetProp-22
    When a user call connect API for device with input file "ConnectCashChanger.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CC_CONNECT_SUCCESS_MESSAGE       |
              
    When a user call SetProperty API to set cash changer property "CurrentExit" with value "2"
    Then error response includes the following
      | Field                               | Value                    |
      | Status Code                         | 400                      |
      | Content-Type                        | application/json         |
      | Status line                         | HTTP/1.1 400             |
      | type                                | Minor                    |
      | errorCode                           | 1204                     |
      | extendedErrorCode                   | 0                        |
      | cc-set-invalid-property-value       | CurrentExit              |
      
    When a user call disconnect API for device with input file "DisconnectCashChanger.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CC_DISCONNECT_SUCCESS_MESSAGE       |