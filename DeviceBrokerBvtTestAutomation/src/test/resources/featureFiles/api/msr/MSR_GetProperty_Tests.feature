Feature: MSR Get Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectMSR.json"
  	
      
  @MSR @BVT @Property @GetProperty
  Scenario: User calls getProperties API when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetProp-1
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetProperties API to get "all" property values for "MSR" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | ParseDecodeData               | true              |
      | CardTypeList                  | BANK              |
      | DeviceAuthenticationProtocol  | 0                 |
      | DecodeData                    | true              |
      | ConnectionStatus              | Connected         |
      | DataEncryptionAlgorithm       | 1                 |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @Property @GetProperty
  Scenario: User calls getProperties API when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetProp-2
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetProperties API to get "DecodeData" property values for "MSR" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | DecodeData                    | true              |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @Property @GetProperty @SetProperty
  Scenario: User calls getProperties API when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetProp-3
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
     
    When a user call SetProperty API to set msr property "DecodeData" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | MSR_SET_PROPERTY_REQUEST_MESSAGE    |
       
    When a user call GetProperties API to get "DecodeData" property values for "MSR" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | DecodeData                    | false             |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @Property @GetProperty
  Scenario: User calls getProperties API when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetProp-4
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetProperties API to get "ParseDecodeData" property values for "MSR" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | ParseDecodeData               | true              |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |