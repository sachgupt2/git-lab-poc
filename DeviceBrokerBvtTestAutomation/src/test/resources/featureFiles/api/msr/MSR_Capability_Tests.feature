Feature: MSR Capability Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectMSR.json"
  	
  @MSR @BVT @Capability
  Scenario: User calls getCapabilities API when MSR device is in disconnected (IDLE) state and verify response.
            This scenario covers test cases DB-MSR-GetCap-1 
   When a user call GetCapabilities API to get "all" capability values for "MSR" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | true                |
     | CapCardAuthentication   | [blank]             |
     | CapDataEncryption       | 1                   |
     | CapDeviceAuthentication | 0                   |
     | CapWritableTracks       | 0                   |
     | CapCardTypeList         | DB_CCL_ISO          |
     | CapTransmitSentinels    | true                |
     | CapTrackDataMasking     | false               |
      
  @MSR @BVT @Capability
  Scenario: User calls getCapabilities API when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetCap-2
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetCapabilities API to get "all" capability values for "MSR" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | true                |
     | CapCardAuthentication   | [blank]             |
     | CapDataEncryption       | 1                   |
     | CapDeviceAuthentication | 0                   |
     | CapWritableTracks       | 0                   |
     | CapCardTypeList         | DB_CCL_ISO          |
     | CapTransmitSentinels    | true                |
     | CapTrackDataMasking     | false               |
     
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @REG @Capability
  Scenario: User calls getCapabilities API to get value for capability 'CapConnection' when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetCap-4
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetCapabilities API to get "CapConnection" capability values for "MSR" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | true                |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @REG @Capability
  Scenario: User calls getCapabilities API to get value for capability 'CapCardTypeList' when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetCap-8
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetCapabilities API to get "CapCardTypeList" capability values for "MSR" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapCardTypeList         | DB_CCL_ISO          |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @REG @Capability
  Scenario: User calls getCapabilities API to get value for capability 'CapTransmitSentinels' when MSR device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-GetCap-10
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call GetCapabilities API to get "CapTransmitSentinels" capability values for "MSR" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapTransmitSentinels    | true                |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |