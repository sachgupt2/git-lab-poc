Feature: MSR Connect Disconnect Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectMSR.json"
  	
  @MSR @BVT @Connect
  Scenario: User calls Connect, and disconnect API for MSR device and verify response.
            This scenario covers test cases DB-MSR-ConDiscon-1 and  DB-MSR-ConDiscon-2
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @REG @Connect
  Scenario: User calls Connect, and disconnect API for MSR device and verify response.
            This scenario covers test cases DB-MSR-ConDiscon-9 and  DB-MSR-ConDiscon-10
    When a user call connect API for device with input file "MultipleConnectRequest.json"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | MULTI_DEVICE_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call disconnect API for device with input file "MultipleDisconnectRequest.json"
    Then response includes the following
      | Field        | Value                                   |
      | Status Code  | 200                                     |
      | Content-Type | application/json                        |
      | Status line  | HTTP/1.1 200                            |
      | message      | MULTI_DEVICE_DISCONNECT_SUCCESS_MESSAGE |