Feature: MSR DirectIo Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectMSR.json"
  	
  @MSR @BVT @DirectIO
  Scenario: User calls DirectIO API for command number '1' for MSR device.
            This scenario covers test cases DB-MSR-directIO-1           

    When a user call DirectIO API for msr with command number "1", data "1" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | [blank]                  | 
      
  @MSR @BVT @DirectIO
  Scenario: User calls DirectIO API for command number '1' for MSR device.
            This scenario covers test cases DB-MSR-directIO-2          

    When a user call DirectIO API for msr with command number "1", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | [blank]                  |
      
   @MSR @BVT @DirectIO
  Scenario: User calls DirectIO API for command number '1' for MSR device.
            This scenario covers test cases DB-MSR-directIO-3           

    When a user call DirectIO API for msr with command number "1", data "" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | additionalData              | [blank]                  | 
     
  @MSR @BVT @DirectIO
  Scenario: User calls DirectIO API for command number '1' for MSR device.
            This scenario covers test cases DB-MSR-directIO-4          

    When a user call DirectIO API for msr with command number "2", data "1" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | [blank]                  |