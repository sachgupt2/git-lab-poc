Feature: MSR Set Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectMSR.json"
      
  @MSR @BVT @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'ParseDecodeData' with value 'true' when MSR is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-SetProp-5
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call SetProperty API to set msr property "ParseDecodeData" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | MSR_SET_PROPERTY_REQUEST_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |
      
  @MSR @BVT @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'DecodeData' with value 'true' when MSR is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-MSR-SetProp-4
    When a user call connect API for device with input file "ConnectMSR.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | MSR_CONNECT_SUCCESS_MESSAGE      |
      
    When a user call SetProperty API to set msr property "DecodeData" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | MSR_SET_PROPERTY_REQUEST_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectMSR.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | MSR_DISCONNECT_SUCCESS_MESSAGE      |