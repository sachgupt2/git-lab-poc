Feature: RFID Get Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectRFID.json"
  	
  @RFID @BVT @Property @GetProperty
  Scenario: User calls getProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetProp-1
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetProperties API to get "all" property values for "RFIDScanner" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | ContinuousReadMode            | false             |
      | ProtocolMask                  | 8                 |
      | ConnectionStatus              | Connected         |
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @Property @GetProperty
  Scenario: User calls getProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetProp-4
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetProperties API to get "ConnectionStatus" property values for "RFIDScanner" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | ConnectionStatus              | Connected         |
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @Property @GetProperty
  Scenario: User calls getProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetProp-8
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
     
    When a user call start read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | startReadTag      |
      | filterId                            | 0                 |
      | filterMask                          | 0                 |
         
    Then start read tag api response includes the following
      | Field             | Value                                |
      | Status Code       | 200                                  |
      | Content-Type      | application/json                     |
      | Status line       | HTTP/1.1 200                         |
      | message           | RFID_START_READ_TAG_REQUEST_MESSAGE  |
       
    When a user call GetProperties API to get "ContinuousReadMode" property values for "RFIDScanner" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | ContinuousReadMode            | true              |
    
    When a user call stop read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | stopReadTag       |
      
    Then stop read tag api response includes the following
      | Field             | Value                                |
      | Status Code       | 200                                  |
      | Content-Type      | application/json                     |
      | Status line       | HTTP/1.1 200                         |
      | message           | RFID_STOP_READ_TAG_REQUEST_MESSAGE   |
              
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @Property @GetProperty
  Scenario: User calls getProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetProp-10
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetProperties API to get "ProtocolMask" property values for "RFIDScanner" device with terminalId "prime55"
    Then get property response includes the following
      | Field                         | Value             |
      | Status Code                   | 200               |
      | Content-Type                  | application/json  |
      | Status line                   | HTTP/1.1 200      |
      | ProtocolMask                  | 8                 |
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |