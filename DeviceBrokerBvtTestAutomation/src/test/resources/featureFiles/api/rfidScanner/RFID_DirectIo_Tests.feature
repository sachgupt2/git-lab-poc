Feature: RFID DirectIo Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectRFID.json"
  	
  @RFID @BVT @DirectIO
  Scenario: User calls DirectIO API for command number '101' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-1
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "101", data "0" and additionaldata ""
    Then error response includes the following
      | Field                | Value                               |
      | Status Code          | 500                                 |
      | Content-Type         | application/json                    |
      | Status line          | HTTP/1.1 500                        |
      | type                 | Major                               |
      | errorCode            | 106                                 |
      | extendedErrorCode    | 0                                   |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE     | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '101' data "1" and additionaldata ""
            This scenario covers test cases DB-RFIDScanner-directIO-2
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "101", data "1" and additionaldata ""
    Then response includes the following
      | Field                       | Value                                          |
      | Status Code                 | 200                                            |
      | Content-Type                | application/json                               |
      | Status line                 | HTTP/1.1 200                                   |
      | data                        | 1                                              |
      | additionalData              | 2>54454355463231343044535250494436382=5553424? | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @DirectIO @REG
  Scenario: User calls DirectIO API for command number '107' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-8
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "107", data "0" and additionaldata ""
    Then error response includes the following
      | Field                | Value                               |
      | Status Code          | 500                                 |
      | Content-Type         | application/json                    |
      | Status line          | HTTP/1.1 500                        |
      | type                 | Major                               |
      | errorCode            | 106                                 |
      | extendedErrorCode    | 0                                   |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE     | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '107' data "0" and additionaldata "030000".
            This scenario covers test cases DB-RFIDScanner-directIO-9
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "107", data "0" and additionaldata "030000"
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 0                   |
      | additionalData              | 030000              | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @DirectIO
  Scenario: User calls DirectIO API for command number '108' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-10
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "108", data "0" and additionaldata ""
    Then error response includes the following
      | Field                | Value                               |
      | Status Code          | 500                                 |
      | Content-Type         | application/json                    |
      | Status line          | HTTP/1.1 500                        |
      | type                 | Major                               |
      | errorCode            | 106                                 |
      | extendedErrorCode    | 0                                   |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE     | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '108' data "0" and additionaldata "1?0000".
            This scenario covers test cases DB-RFIDScanner-directIO-11
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "108", data "0" and additionaldata "1?0000"
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 0                   |
      | additionalData              | 1?0000              | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '113' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-12
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "113", data "0" and additionaldata ""
    Then error response includes the following
      | Field                | Value                               |
      | Status Code          | 500                                 |
      | Content-Type         | application/json                    |
      | Status line          | HTTP/1.1 500                        |
      | type                 | Major                               |
      | errorCode            | 106                                 |
      | extendedErrorCode    | 0                                   |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE     | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '114' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-13
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "114", data "0" and additionaldata ""
    Then error response includes the following
      | Field                | Value                               |
      | Status Code          | 500                                 |
      | Content-Type         | application/json                    |
      | Status line          | HTTP/1.1 500                        |
      | type                 | Major                               |
      | errorCode            | 106                                 |
      | extendedErrorCode    | 0                                   |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE     | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '115' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-14
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "115", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 128                 |
      | additionalData              | [blank]             | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '116' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-15
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "116", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 0                   |
      | additionalData              | [blank]             | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '117' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-16
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "117", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 0                   |
      | additionalData              | [blank]             | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '118' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-17
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "118", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 0                   |
      | additionalData              | [blank]             | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '119' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-18
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "119", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value               |
      | Status Code                 | 200                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 200        |
      | data                        | 0                   |
      | additionalData              | [blank]             | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @DirectIO
  Scenario: User calls DirectIO API for command number '120' data "0" and additionaldata "".
            This scenario covers test cases DB-RFIDScanner-directIO-19
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call DirectIO API for RFIDScanner with command number "120", data "0" and additionaldata ""
    Then error response includes the following
      | Field                | Value                               |
      | Status Code          | 500                                 |
      | Content-Type         | application/json                    |
      | Status line          | HTTP/1.1 500                        |
      | type                 | Major                               |
      | errorCode            | 106                                 |
      | extendedErrorCode    | 0                                   |
      | message              | ILLEGAL_OPERATION_ERROR_MESSAGE     | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     | 