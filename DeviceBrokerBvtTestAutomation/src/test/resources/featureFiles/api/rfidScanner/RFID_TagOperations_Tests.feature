Feature: RFID Tag Operation Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectRFID.json"
  	
  @RFID @BVT @StartReadTag
  Scenario: User calls readTag API for RFID Scanner device and verify response and tag data event.
            This scenario covers test cases DB-RFIDScanner-StopReadTag-1, DB-RFIDScanner-StopReadTag-3
    When a user call stop read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | stopReadTag       |
      
    Then error response includes the following
      | Field                | Value                                               |
      | Status Code          | 400                                                 |
      | Content-Type         | application/json                                    |
      | Status line          | HTTP/1.1 400                                        |
      | type                 | Major                                               |
      | errorCode            | 1305                                                |
      | extendedErrorCode    | 0                                                   |
      | message              | RFID_STOP_READ_TAG_WITH_OUT_CONNECT_ERROR_MESSAGE   |
  
  @RFID @BVT @StartReadTag
  Scenario: User calls readTag API for RFID Scanner device and verify response and tag data event.
            This scenario covers test cases DB-RFIDScanner-StartReadTag-1
    When a user call start read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | startReadTag      |
      | filterId                            | 0                 |
      | filterMask                          | 0                 |
         
    Then error response includes the following
      | Field                | Value                                               |
      | Status Code          | 400                                                 |
      | Content-Type         | application/json                                    |
      | Status line          | HTTP/1.1 400                                        |
      | type                 | Major                                               |
      | errorCode            | 1305                                                |
      | extendedErrorCode    | 0                                                   |
      | message              | RFID_START_READ_TAG_WITH_OUT_CONNECT_ERROR_MESSAGE  |
          
  @RFID @BVT @ReadTag
  Scenario: User calls readTag API for RFID Scanner device and verify response and tag data event.
            This scenario covers test cases DB-RFIDScanner-ReadTagID-1, DB-RFIDScanner-ReadTagID-39
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
     
    Given Connect to message broker 
    Then Subscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"  
    
    When a user call read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | readTag           |
      | filterId                            | 0                 |
      | filterMask                          | 0                 |
         
    Then read tag api response includes the following
      | Field             | Value                          |
      | Status Code       | 200                            |
      | Content-Type      | application/json               |
      | Status line       | HTTP/1.1 200                   |
      | message           | RFID_READ_TAG_REQUEST_MESSAGE  |
      
    Then In 2 seconds should receive message
    """
    {
    		"actions": [
        		{
            		"actionType": "readTag",
            		"eventBody": {
                		"rfidScannerDataList": [
                    		{
                        		"tagId": "303515A5240289C081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000004",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402894081000003",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000002",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		}
                		]
            		}
        		}
    		]
		}
    """
    And Unsubscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"
    And Disconnect Broker
    
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @ReadTag @REG
  Scenario: User calls readTag API for RFID Scanner device and verify response and tag data event.
            This scenario covers test cases DB-RFIDScanner-ReadTagID-2
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
     
    Given Connect to message broker 
    Then Subscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"  
    
    When a user call read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | readTag           |
      | filterId                            | 0                 |
      | filterMask                          | 0                 |
         
    Then read tag api response includes the following
      | Field             | Value                          |
      | Status Code       | 200                            |
      | Content-Type      | application/json               |
      | Status line       | HTTP/1.1 200                   |
      | message           | RFID_READ_TAG_REQUEST_MESSAGE  |
      
    Then In 2 seconds should receive message
    """
    {
    		"actions": [
        		{
            		"actionType": "readTag",
            		"eventBody": {
                		"rfidScannerDataList": [
                    		{
                        		"tagId": "303515A5240289C081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000004",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402894081000003",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000002",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		}
                		]
            		}
        		}
    		]
		}
    """
    When a user call read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | readTag           |
      | filterId                            | 0                 |
      | filterMask                          | 0                 |
         
    Then read tag api response includes the following
      | Field             | Value                          |
      | Status Code       | 200                            |
      | Content-Type      | application/json               |
      | Status line       | HTTP/1.1 200                   |
      | message           | RFID_READ_TAG_REQUEST_MESSAGE  |
      
    Then In 2 seconds should receive message
    """
		{
    		"actions": [
        		{
            		"actionType": "readTag",
            		"eventBody": {
                		"rfidScannerDataList": [
                    		{
                        		"tagId": "303515A5240289C081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000004",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402894081000003",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000002",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		}
                		]
            		}
        		}
    		]
		}
    """
    And Unsubscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"
    And Disconnect Broker
    
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @WriteTag
  Scenario: User calls writeTag API for RFID Scanner device and verify response and tag data event.
            This scenario covers test cases DB-RFIDScanner-WriteTagID-1
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
     
    Given Connect to message broker 
    Then Subscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"  
    
    When a user call write tag API with following values in table
      | Field                               | Value                    |
      | terminalID                          | prime55                  |
      | category                            | RFIDScanner              |
      | logicalName                         | RFIDScanner              |
      | operationType                       | writeTagId               |
      | sourceID                            | 303515A52402898081000001 |
      | destID                              | 303515A52402898081000001 |
         
    Then write tag api response includes the following
      | Field             | Value                          |
      | Status Code       | 200                            |
      | Content-Type      | application/json               |
      | Status line       | HTTP/1.1 200                   |
      | message           | RFID_WRITE_TAG_REQUEST_MESSAGE |
      
    Then In 2 seconds should receive message
    """
			{
    		"actions": [
        		{
            		"actionType": "writeTagId",
            		"eventBody": {
              	  	"resultCode": 0,
               		 	"message": "Device RFIDScanner operation writeTagId successfully completed."
            		}
        		}
    		]
			}
    """
    And Unsubscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"
    And Disconnect Broker
    
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @StartReadTag
  Scenario: User calls readTag API for RFID Scanner device and verify response and tag data event.
            This scenario covers test cases DB-RFIDScanner-StartReadTag-4, DB-RFIDScanner-StopReadTag-5
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
     
    Given Connect to message broker 
    Then Subscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"  
    When a user call start read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | startReadTag      |
      | filterId                            | 0                 |
      | filterMask                          | 0                 |
         
    Then start read tag api response includes the following
      | Field             | Value                                |
      | Status Code       | 200                                  |
      | Content-Type      | application/json                     |
      | Status line       | HTTP/1.1 200                         |
      | message           | RFID_START_READ_TAG_REQUEST_MESSAGE  |
    
    Then In 2 seconds should receive message
    """
    {
    		"actions": [
        		{
            		"actionType": "startReadTag",
            		"eventBody": {
                		"rfidScannerDataList": [
                    		{
                        		"tagId": "303515A5240289C081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000004",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402894081000003",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000002",
                        		"userdata": "",
                        		"protocol": 8
                    		},
                    		{
                        		"tagId": "303515A52402898081000001",
                        		"userdata": "",
                        		"protocol": 8
                    		}
                		]
            		}
        		}
    		]
		}
    """  
    When a user call stop read tag API with following values in table
      | Field                               | Value             |
      | terminalID                          | prime55           |
      | category                            | RFIDScanner       |
      | logicalName                         | RFIDScanner       |
      | operationType                       | stopReadTag       |
      
    Then stop read tag api response includes the following
      | Field             | Value                                |
      | Status Code       | 200                                  |
      | Content-Type      | application/json                     |
      | Status line       | HTTP/1.1 200                         |
      | message           | RFID_STOP_READ_TAG_REQUEST_MESSAGE   |      
    
    And Unsubscribe topic "shared.bus.001.service.ngp.device.rfidscanner.events"
    And Disconnect Broker
       
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
      
  