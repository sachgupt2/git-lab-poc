Feature: RFID Set Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectRFID.json"
  
  @RFID @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-SetProp-1
    When a user call SetProperty API to set RFIDScanner property "ConnectionStatus" with value "false"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | RFID_SET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE     | 
     
  @RFID @BVT @Property @SetProperty
  Scenario: User calls setProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-SetProp-2
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call SetProperty API to set RFIDScanner property "ConnectionStatus" with value ""
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | rfid-set-read-only-property     | ConnectionStatus         | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-SetProp-6
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call SetProperty API to set RFIDScanner property "ConnectionStatus" with value "true"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | rfid-set-read-only-property     | ConnectionStatus         | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-SetProp-9
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call SetProperty API to set RFIDScanner property "ContinuousReadMode" with value "true"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | rfid-set-read-only-property     | ContinuousReadMode       | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-SetProp-11
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call SetProperty API to set RFIDScanner property "ProtocolMask" with value "1"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | RFID_SET_PROPERTY_REQUEST_MESSAGE   | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @Property @SetProperty
  Scenario: User calls setProperties API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-SetProp-20
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call SetProperty API to set RFIDScanner property "ProtocolMask" with value "16777216"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | RFID_SET_PROPERTY_REQUEST_MESSAGE   | 
            
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |