Feature: RFID Capability Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectRFID.json"
  	
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in dis-connected (IDLE) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-4
    When a user call GetCapabilities API to get "all" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | false               |
     | CapContinuousRead       | true                |
     | CapDisableTag           | true                |
     | CapWriteTag             | 3                   |
     | CapLockTag              | true                |
     | CapMultipleProtocols    | 8                   |
     | CapReadTimer            | true                |
      
  @RFID @BVT @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-5
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetCapabilities API to get "all" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | false               |
     | CapContinuousRead       | true                |
     | CapDisableTag           | true                |
     | CapWriteTag             | 3                   |
     | CapLockTag              | true                |
     | CapMultipleProtocols    | 8                   |
     | CapReadTimer            | true                |
      
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in dis-connected (IDLE) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-8
    When a user call GetCapabilities API to get "CapConnection" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | false               |
     
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-9
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetCapabilities API to get "CapConnection" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapConnection           | false               |
      
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
     
  @RFID @BVT @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in dis-connected (IDLE) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-14
    When a user call GetCapabilities API to get "CapContinuousRead" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapContinuousRead        | true                |
 
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-15
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetCapabilities API to get "CapContinuousRead" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapContinuousRead        | true                |
      
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
      
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-21
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetCapabilities API to get "CapDisableTag" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapDisableTag           | true                |
      
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     |
 
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in dis-connected (IDLE) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-26
    When a user call GetCapabilities API to get "CapLockTag" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapLockTag              | true                |
     
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in dis-connected (IDLE) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-32
    When a user call GetCapabilities API to get "CapReadTimer" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapReadTimer            | true                |
 
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in dis-connected (IDLE) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-38
    When a user call GetCapabilities API to get "CapWriteTag" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapWriteTag             | 3                   |
 
  @RFID @BVT @REG @Capability
  Scenario: User calls getCapabilities API when RFID Scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-RFIDScanner-GetCap-45
    When a user call connect API for device with input file "ConnectRFID.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | RFID_CONNECT_SUCCESS_MESSAGE     |
      
    When a user call GetCapabilities API to get "CapMultipleProtocols" capability values for "RFIDScanner" device
    Then get capability response includes the following
     | Field                   | Value               |
     | Status Code             | 200                 |
     | Content-Type            | application/json    |
     | Status line             | HTTP/1.1 200        |
     | CapMultipleProtocols    | 8                   |
      
    When a user call disconnect API for device with input file "DisconnectRFID.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | RFID_DISCONNECT_SUCCESS_MESSAGE     | 