Feature: Printer Connect Disconnect Tests

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectPrinter.json"
		
  @BVT @Printer @Connect
  Scenario: User calls Connect, Print and disconnect API for printer device and verify response.
            This scenario covers test cases MAN_PRIN_01, MAN_PRIN_02,  MAN_PRIN_03, MAN_PRIN_55, MAN_PRIN_13, MAN_PRIN_83, MAN_PRIN_84

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

      When a user call Print API for device with input file "Print.json"
      Then response includes the following
      | Field        | Value                          |
      | Status Code  | 200                            |
      | Content-Type | application/json               |
      | Status line  | HTTP/1.1 200                   |
      | message      | PRINTER_PRINT_SUCCESS_MESSAGE  |
      
      When a user call Print API for device with input file "PrintWithDifferentTerminalId.json"
      Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 409                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 409                                         |
      | type                 | Minor                                                |
      | errorCode            | 1104                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | PRINTER_DIFFERENT_TERMINAL_ID_PRINT_REQUEST_MESSAGE  |
      
      When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |

	@BVT @Printer @Connect @REG
  Scenario: User calls Connect API 2 times with same terminal id and verify response.
            This scenario covers test cases MAN_PRIN_03
    When a user call connect API for device with input file "ConnectPrinter.json"
    When a user call connect API for device with input file "ConnectPrinter.json"
    Then error response includes the following
      | Field                | Value                                      |
      | Status Code          | 409                                        |
      | Content-Type         | application/json                           |
      | Status line          | HTTP/1.1 409                               |
      | type                 | Minor                                      |
      | errorCode            | 1115                                       |
      | extendedErrorCode    | 0                                          |
      | message              | PRINTER_DUPLICATE_CONNECT_REQUEST_MESSAGE  |

      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @Connect @REG
  Scenario: User calls Connect API 2 times with different terminal id and verify response.
            This scenario covers test cases MAN_PRIN_09
    When a user call connect API for device with input file "ConnectPrinter.json"
    When a user call connect API for device with input file "ConnectPrinterDifferentTerminalId.json"
    Then error response includes the following
      | Field                | Value                                                  |
      | Status Code          | 409                                                    |
      | Content-Type         | application/json                                       |
      | Status line          | HTTP/1.1 409                                           |
      | type                 | Minor                                                  |
      | errorCode            | 1101                                                   |
      | extendedErrorCode    | 0                                                      |
      | message              | PRINTER_DIFFERENT_TERMINAL_ID_CONNECT_REQUEST_MESSAGE  |

      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @Connect @REG
  Scenario: User calls disconnect API with different terminal id and verify response.
            This scenario covers test cases MAN_PRIN_10
    When a user call connect API for device with input file "ConnectPrinter.json"
    When a user call disconnect API for device with input file "DisconnectPrinterDifferentTerminalId.json"
    Then error response includes the following
      | Field                | Value                                                     |
      | Status Code          | 409                                                       |
      | Content-Type         | application/json                                          |
      | Status line          | HTTP/1.1 409                                              |
      | type                 | Minor                                                     |
      | errorCode            | 1104                                                      |
      | extendedErrorCode    | 0                                                         |
      | message              | PRINTER_DIFFERENT_TERMINAL_ID_DISCONNECT_REQUEST_MESSAGE  |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |