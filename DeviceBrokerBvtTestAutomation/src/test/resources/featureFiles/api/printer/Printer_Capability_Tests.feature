Feature: Printer Capabilities Tests

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectPrinter.json"
  	
  
  @BVT @Printer @Capability
  Scenario: User calls getCapability API for printer device and verify response.
            This scenario covers test cases MAN_PRIN_95          

    When a user call GetCapabilities API to get "CapConnection" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                 |
     | Status Code         | 200                   |
     | Content-Type        | application/json      |
     | Status line         | HTTP/1.1 200          |
     | CapConnection       | true                  |
     	
  @BVT @Printer @Capability
  Scenario: User calls getCapability  API when printer is in connected state (BUSY) and verify response.
            This scenario covers test cases MAN_PRIN_87          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "all" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapConnection       | true                                                                         |
     | CapCharacterSet     | DB_CCS_KANJI                                                                 |
     | CapStation          | [DB_CS_RECEIPT]                                                              |
     | CapConcurrent       | DB_CC_NONE                                                                   |
     | CapMapCharacterSet  | false                                                                        |
     | CapTransaction      | true                                                                         |
     | CapPaperSensor      | [DB_CPS_EMPTY, DB_CPS_NEAR_EMPTY]                                            |
     | CapFormat           | [DB_CF_BOLD, DB_CF_DWIDE, DB_CF_DWIDE_DHIGH, DB_CF_DHIGH, DB_CF_UNDERLINE]   |
     | CapCartridgeSensor  | DB_CCS_CART_OK                                                               |
     | CapColor            | DB_COLOR_NONE                                                                |
     | CapBitmap           | true                                                                         |
     | CapBarCode          | true                                                                         |
     | CapRotation         | [DB_CR_LEFT_90, DB_CR_RIGHT_90, DB_CR_ROTATE_180]                            |
     | CapPageMode         | true                                                                         |
     | CapCoverSensor      | true                                                                         |
     | CapMarkFeed         | DB_MF_NONE                                                                   |
     | CapPaperCut         | true                                                                         |
     
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
  
  @BVT @Printer @Capability
  Scenario: User calls getCapability API when printer is in disconnected state (IDLE) and verify response.
            This scenario covers test cases MAN_PRIN_88, MAN_PRIN_90          

    When a user call GetCapabilities API to get "all" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapConnection       | true                                                                         |
     | CapCharacterSet     | DB_CCS_KANJI                                                                 |
     | CapStation          | [DB_CS_RECEIPT]                                                              |
     | CapConcurrent       | DB_CC_NONE                                                                   |
     | CapMapCharacterSet  | false                                                                        |
     | CapTransaction      | true                                                                         |
     | CapPaperSensor      | [DB_CPS_EMPTY, DB_CPS_NEAR_EMPTY]                                            |
     | CapFormat           | [DB_CF_BOLD, DB_CF_DWIDE, DB_CF_DWIDE_DHIGH, DB_CF_DHIGH, DB_CF_UNDERLINE]   |
     | CapCartridgeSensor  | DB_CCS_CART_OK                                                               |
     | CapColor            | DB_COLOR_NONE                                                                |
     | CapBitmap           | true                                                                         |
     | CapBarCode          | true                                                                         |
     | CapRotation         | [DB_CR_LEFT_90, DB_CR_RIGHT_90, DB_CR_ROTATE_180]                            |
     | CapPageMode         | true                                                                         |
     | CapCoverSensor      | true                                                                         |
     | CapMarkFeed         | DB_MF_NONE                                                                   |
     | CapPaperCut         | true                                                                         |


  @BVT @Printer @Capability
  Scenario: User calls Connect, getCapability and disconnect API for printer device and verify response.
            This scenario covers test cases MAN_PRIN_94          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapConnection" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapConnection       | true                                                                         |
     
      
     When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
  
  
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API with invalid capability name for printer device and verify response.
            This scenario covers test cases MAN_PRIN_93          

    When a user call GetCapabilities API to get "ABC" capability values for "POSPrinter" device
	  Then error response includes the following
      | Field                | Value                                                          |
      | Status Code          | 400                                                            |
      | Content-Type         | application/json                                               |
      | Status line          | HTTP/1.1 400                                                   |
      | type                 | Minor                                                          |
      | errorCode            | 1204                                                           |
      | extendedErrorCode    | 0                                                              |
      | message              | PRINTER_GET_CAPABILITY_WITH_INVALID_CAPABILITY_NAME_MESSAGE    |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapCharacterSet' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_98          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapCharacterSet" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapCharacterSet     | DB_CCS_KANJI                                                                 |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapCharacterSet' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_99          

    When a user call GetCapabilities API to get "CapCharacterSet" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapCharacterSet     | DB_CCS_KANJI                                                                 |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapStation' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_102          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapStation" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapStation          | [DB_CS_RECEIPT]                                                              |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapStation' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_103          

    When a user call GetCapabilities API to get "CapStation" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapStation          | [DB_CS_RECEIPT]                                                              |
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapConcurrent' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_106          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapConcurrent" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                                        |
     | Status Code         | 200                                                                          |
     | Content-Type        | application/json                                                             |
     | Status line         | HTTP/1.1 200                                                                 |
     | CapConcurrent       | DB_CC_NONE                                                                   |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapConcurrent' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_107          

    When a user call GetCapabilities API to get "CapConcurrent" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                                                        |
     | Status Code            | 200                                                                          |
     | Content-Type           | application/json                                                             |
     | Status line            | HTTP/1.1 200                                                                 |
     | CapConcurrent          | DB_CC_NONE                                                                   |
	
	
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapMapCharacterSet' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_110          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapMapCharacterSet" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                      |
     | Status Code         | 200                        |
     | Content-Type        | application/json           |
     | Status line         | HTTP/1.1 200               |
     | CapMapCharacterSet  | false                      |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapMapCharacterSet' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_111          

    When a user call GetCapabilities API to get "CapMapCharacterSet" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                     |
     | Status Code            | 200                       |
     | Content-Type           | application/json          |
     | Status line            | HTTP/1.1 200              |
     | CapMapCharacterSet     | false                     |
   
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapTransaction' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_114          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapTransaction" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                      |
     | Status Code         | 200                        |
     | Content-Type        | application/json           |
     | Status line         | HTTP/1.1 200               |
     | CapTransaction      | true                       |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapTransaction' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_115          

    When a user call GetCapabilities API to get "CapTransaction" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                     |
     | Status Code            | 200                       |
     | Content-Type           | application/json          |
     | Status line            | HTTP/1.1 200              |
     | CapTransaction         | true                      |
    
    
 
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API with invalid capability name 'CapSlpBothSidesPrint' when printer is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_118          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
    
    When a user call GetCapabilities API to get "CapSlpBothSidesPrint" capability values for "POSPrinter" device
	  Then error response includes the following
      | Field                | Value                                                          |
      | Status Code          | 400                                                            |
      | Content-Type         | application/json                                               |
      | Status line          | HTTP/1.1 400                                                   |
      | type                 | Minor                                                          |
      | errorCode            | 1204                                                           |
      | extendedErrorCode    | 0                                                              |
      | message              | PRINTER_GET_CAPABILITY_BOTH_SIDE_PRINT_MESSAGE                 |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
 
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API with invalid capability name 'CapSlpBothSidesPrint' when printer is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_119          

    When a user call GetCapabilities API to get "CapSlpBothSidesPrint" capability values for "POSPrinter" device
	  Then error response includes the following
      | Field                | Value                                                          |
      | Status Code          | 400                                                            |
      | Content-Type         | application/json                                               |
      | Status line          | HTTP/1.1 400                                                   |
      | type                 | Minor                                                          |
      | errorCode            | 1204                                                           |
      | extendedErrorCode    | 0                                                              |
      | message              | PRINTER_GET_CAPABILITY_BOTH_SIDE_PRINT_MESSAGE                 |


  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapPaperSensor' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_122          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapPaperSensor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                   |
     | Status Code         | 200                                                     |
     | Content-Type        | application/json                                        |
     | Status line         | HTTP/1.1 200                                            |
     | CapPaperSensor      | [DB_CPS_EMPTY, DB_CPS_NEAR_EMPTY]                       |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapPaperSensor' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_123          

    When a user call GetCapabilities API to get "CapPaperSensor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                                  |
     | Status Code            | 200                                                    |
     | Content-Type           | application/json                                       |
     | Status line            | HTTP/1.1 200                                           |
     | CapPaperSensor         | [DB_CPS_EMPTY, DB_CPS_NEAR_EMPTY]                      |
     
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapCoverSensor' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_134          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapCoverSensor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field               | Value                                                   |
     | Status Code         | 200                                                     |
     | Content-Type        | application/json                                        |
     | Status line         | HTTP/1.1 200                                            |
     | CapCoverSensor      | true                                                    |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapCoverSensor' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_135          

    When a user call GetCapabilities API to get "CapCoverSensor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                                  |
     | Status Code            | 200                                                    |
     | Content-Type           | application/json                                       |
     | Status line            | HTTP/1.1 200                                           |
     | CapCoverSensor         | true                                                   |
  	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapFormat' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_138          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapFormat" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                                                       |
     | Status Code            | 200                                                                         |
     | Content-Type           | application/json                                                            |
     | Status line            | HTTP/1.1 200                                                                |
     | CapFormat              | [DB_CF_BOLD, DB_CF_DWIDE, DB_CF_DWIDE_DHIGH, DB_CF_DHIGH, DB_CF_UNDERLINE]  |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapFormat' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_139          

    When a user call GetCapabilities API to get "CapFormat" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                                                       |
     | Status Code            | 200                                                                         |
     | Content-Type           | application/json                                                            |
     | Status line            | HTTP/1.1 200                                                                |
     | CapFormat              | [DB_CF_BOLD, DB_CF_DWIDE, DB_CF_DWIDE_DHIGH, DB_CF_DHIGH, DB_CF_UNDERLINE]  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapCartridgeSensor' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_142          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapCartridgeSensor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapCartridgeSensor     | DB_CCS_CART_OK          |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapCartridgeSensor' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_143          

    When a user call GetCapabilities API to get "CapCartridgeSensor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapCartridgeSensor     | DB_CCS_CART_OK          |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapColor' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_146          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapColor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapColor               | DB_COLOR_NONE           |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapColor' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_147          

    When a user call GetCapabilities API to get "CapColor" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapColor               | DB_COLOR_NONE           |
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapBitmap' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_150          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapBitmap" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapBitmap              | true                    |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapBitmap' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_151          

    When a user call GetCapabilities API to get "CapBitmap" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapBitmap              | true                    |
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapBarCode' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_154          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapBarCode" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapBarCode             | true                    |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapBarCode' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_155         

    When a user call GetCapabilities API to get "CapBarCode" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapBarCode             | true                    |
	
	
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapMarkFeed' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_158          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapMarkFeed" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapMarkFeed            | DB_MF_NONE              |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapMarkFeed' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_159         

    When a user call GetCapabilities API to get "CapMarkFeed" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                   |
     | Status Code            | 200                     |
     | Content-Type           | application/json        |
     | Status line            | HTTP/1.1 200            |
     | CapMarkFeed            | DB_MF_NONE              |
  	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapRotation' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_162          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapRotation" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                             |
     | Status Code            | 200                                               |
     | Content-Type           | application/json                                  |
     | Status line            | HTTP/1.1 200                                      |
     | CapRotation            | [DB_CR_LEFT_90, DB_CR_RIGHT_90, DB_CR_ROTATE_180] |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapRotation' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_163         

    When a user call GetCapabilities API to get "CapRotation" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                             |
     | Status Code            | 200                                               |
     | Content-Type           | application/json                                  |
     | Status line            | HTTP/1.1 200                                      |
     | CapRotation            | [DB_CR_LEFT_90, DB_CR_RIGHT_90, DB_CR_ROTATE_180] |
	
	
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapPaperCut' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_166          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapPaperCut" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                             |
     | Status Code            | 200                                               |
     | Content-Type           | application/json                                  |
     | Status line            | HTTP/1.1 200                                      |
     | CapPaperCut            | true                                              |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapPaperCut' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_167         

    When a user call GetCapabilities API to get "CapPaperCut" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                             |
     | Status Code            | 200                                               |
     | Content-Type           | application/json                                  |
     | Status line            | HTTP/1.1 200                                      |
     | CapPaperCut            | true                                              |
  	
	
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapPageMode' when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_170          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetCapabilities API to get "CapPageMode" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                             |
     | Status Code            | 200                                               |
     | Content-Type           | application/json                                  |
     | Status line            | HTTP/1.1 200                                      |
     | CapPageMode            | true                                              |
     
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
	@BVT @Printer @Capability @REG
  Scenario: User calls getCapability API to get value of capability 'CapPageMode' when printer device is in disconnected state and verify response.
            This scenario covers test cases MAN_PRIN_171         

    When a user call GetCapabilities API to get "CapPageMode" capability values for "POSPrinter" device
    Then get capability response includes the following
     | Field                  | Value                                             |
     | Status Code            | 200                                               |
     | Content-Type           | application/json                                  |
     | Status line            | HTTP/1.1 200                                      |
     | CapPageMode            | true                                              |
     
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API with invalid capability 'CapCompareFirmwareVersion' for printer device and verify response.
            This scenario covers test cases MAN_PRIN_361          

    When a user call GetCapabilities API to get "CapCompareFirmwareVersion" capability values for "POSPrinter" device
	  Then error response includes the following
      | Field                | Value                                                                      |
      | Status Code          | 400                                                                        |
      | Content-Type         | application/json                                                           |
      | Status line          | HTTP/1.1 400                                                               |
      | type                 | Minor                                                                      |
      | errorCode            | 1204                                                                       |
      | extendedErrorCode    | 0                                                                          |
      | message              | PRINTER_GET_CAPABILITY_WITH_INVALID_CAPABILITY_FIRMWARE_VERSION_MESSAGE    |
      
  @BVT @Printer @Capability @REG
  Scenario: User calls getCapability API with invalid capability 'CapUpdateFirmware' for printer device and verify response.
            This scenario covers test cases MAN_PRIN_362          

    When a user call GetCapabilities API to get "CapUpdateFirmware" capability values for "POSPrinter" device
	  Then error response includes the following
      | Field                | Value                                                                     |
      | Status Code          | 400                                                                       |
      | Content-Type         | application/json                                                          |
      | Status line          | HTTP/1.1 400                                                              |
      | type                 | Minor                                                                     |
      | errorCode            | 1204                                                                      |
      | extendedErrorCode    | 0                                                                         |
      | message              | PRINTER_GET_CAPABILITY_WITH_INVALID_CAPABILITY_UPDATE_FIRMWARE_MESSAGE    |