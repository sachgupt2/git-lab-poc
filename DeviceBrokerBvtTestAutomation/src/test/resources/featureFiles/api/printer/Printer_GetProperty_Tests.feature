Feature: Printer Get Property Tests

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectPrinter.json"
  
  @BVT @Printer @Property @GetProperty
  Scenario: User calls Connect, GetProperty and disconnect API for printer device and verify response.
            This scenario covers test cases MAN_PRIN_174
            
    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "all" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLineSpacing              | 30                       |
	    | RecLineChars                | 32                       |
	    | RecLineWidth                | 416                      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
  
  @BVT @Printer @Property @GetProperty
  Scenario: User calls GetProperty API with out calling connect method for printer device and verify response.
            This scenario covers test cases MAN_PRIN_175
            
    When a user call GetProperties API to get "all" property values for "POSPrinter" device with terminalId "prime55"
	  Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | PRINTER_GET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE  |
  
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API when printer is in disconnected state (IDLE) and verify response.
            This scenario covers test cases MAN_PRIN_177          

    When a user call GetProperties API to get "all" property values for "POSPrinter" device with terminalId "prime55"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | PRINTER_GET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE  |
  
  
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API from different terminal Id  when printer device is in connected state and verify response.
            This scenario covers test cases MAN_PRIN_179          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "all" property values for "POSPrinter" device with terminalId "prime66"
    Then error response includes the following
      | Field                | Value                                                       |
      | Status Code          | 409                                                         |
      | Content-Type         | application/json                                            |
      | Status line          | HTTP/1.1 409                                                |
      | type                 | Minor                                                       |
      | errorCode            | 1104                                                        |
      | extendedErrorCode    | 0                                                           |
      | message              | PRINTER_DIFFERENT_TERMINAL_ID_GET_PROPERTY_REQUEST_MESSAGE  |
     
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API with invalid logical name and verify response.
            This scenario covers test cases MAN_PRIN_181          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "all" property values for "POSPrinter123" device with terminalId "prime55"
    Then error response includes the following
      | Field                | Value                                                       |
      | Status Code          | 400                                                         |
      | Content-Type         | application/json                                            |
      | Status line          | HTTP/1.1 400                                                |
      | type                 | Major                                                       |
      | errorCode            | 1105                                                        |
      | extendedErrorCode    | 0                                                           |
      | message              | PRINTER_DEVICE_NOT_FOUND_ERROR_MESSAGE                      |
     
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
  
  @BVT @Printer @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'ConnectionStatus' when printer is in connected state (BUSY) and verify response.
            This scenario covers test cases MAN_PRIN_183
            
    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "ConnectionStatus" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | ConnectionStatus            | Connected                |
	    
      When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'ConnectionStatus' when printer is in disconnected state (IDLE) and verify response.
            This scenario covers test cases MAN_PRIN_184          

    When a user call GetProperties API to get "ConnectionStatus" property values for "POSPrinter" device with terminalId "prime55"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | PRINTER_GET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE  |
      
  @BVT @Printer @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'AsyncMode' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_187
            
    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "AsyncMode" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | AsyncMode                   | false                    |
	    
      When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'CharacterSet' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_188          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "CharacterSet" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | CharacterSet                | 932                      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'CharacterSetList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_189          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "CharacterSetList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | CharacterSetList            | 437,932,737,850,999      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'CoverOpen' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_191          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "CoverOpen" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | CoverOpen                   | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'ErrorStationList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_192          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "ErrorStationList" property values for "POSPrinter" device with terminalId "prime55"
    Then error response includes the following
      | Field                       | Value               |
      | Status Code                 | 400                 |
      | Content-Type                | application/json    |
      | Status line                 | HTTP/1.1 400        |
      | type                        | Minor               |
      | errorCode                   | 1204                |
      | extendedErrorCode           | 0                   |
      | PrinterPropertyNotSupported | ErrorStationList    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'FontTypefaceList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_193          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "FontTypefaceList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | FontTypefaceList            | FontA,Condense,FontB     |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnCurrentCartridge' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_194          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnCurrentCartridge" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnCurrentCartridge         | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnEmpty' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_195          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnEmpty" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnEmpty                    | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnLetterQuality' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_196          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnLetterQuality" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnLetterQuality            | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnLineChars' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_197          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnLineChars" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnLineChars                | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnLineCharsList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_198          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnLineCharsList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnLineCharsList            | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnLineHeight' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_199          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnLineHeight" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnLineHeight               | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnLineSpacing' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_200          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnLineSpacing" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnLineSpacing              | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnLineWidth' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_201          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnLineWidth" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnLineWidth                | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'JrnNearEnd' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_202          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "JrnNearEnd" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | JrnNearEnd                  | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'MapCharacterSet' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_203          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "MapCharacterSet" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | MapCharacterSet             | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'MapMode' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_204          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "MapMode" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | MapMode                     | 1                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModeArea' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_205          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModeArea" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModeArea                | [blank]                  |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModeDescriptor' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_206          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModeDescriptor" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModeDescriptor          | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModeHorizontalPosition' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_207          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModeHorizontalPosition" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModeHorizontalPosition  | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModePrintArea' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_208          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModePrintArea" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModePrintArea           | 00,00,00,00,00,00,00,00  |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModePrintDirection' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_209          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModePrintDirection" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModePrintDirection      | 1                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModeStation' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_210          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModeStation" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModeStation             | 2                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'PageModeVerticalPosition' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_211          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "PageModeVerticalPosition" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | PageModeVerticalPosition    | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecBarCodeRotationList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_212          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecBarCodeRotationList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecBarCodeRotationList      | 0,180                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecBitmapRotationList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_213          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecBitmapRotationList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecBitmapRotationList       | 0,R90,L90,180            |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecCartridgeState' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_214          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecCartridgeState" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecCartridgeState           | 268435456                |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecCurrentCartridge' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_215          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecCurrentCartridge" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecCurrentCartridge         | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecEmpty' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_216          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecEmpty" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecEmpty                    | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLetterQuality' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_217          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLetterQuality" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLetterQuality            | true                     |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLineChars' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_218          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLineChars" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLineChars                | 32                       |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLineCharsList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_219          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLineCharsList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLineCharsList            | 32,41,46                 |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLineHeight' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_220          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLineHeight" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLineHeight               | 24                       |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLineSpacing' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_221          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLineSpacing" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLineSpacing              | 30                       |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLinesToPaperCut' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_222          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLinesToPaperCut" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLinesToPaperCut          | 7                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecLineWidth' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_223          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecLineWidth" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecLineWidth                | 416                      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecNearEnd' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_224          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecNearEnd" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecNearEnd                  | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecSidewaysMaxChars' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_225          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecSidewaysMaxChars" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecSidewaysMaxChars         | 184                      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RecSidewaysMaxLines' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_226          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RecSidewaysMaxLines" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RecSidewaysMaxLines         | 16                       |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'RotateSpecial' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_227          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "RotateSpecial" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RotateSpecial               | 1                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpBarCodeRotationList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_228          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpBarCodeRotationList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpBarCodeRotationList      | [blank]                  |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpBitmapRotationList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_229          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpBitmapRotationList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpBitmapRotationList       | [blank]                  |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpCartridgeState' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_230          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpCartridgeState" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpCartridgeState           | 268435456                |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpCurrentCartridge' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_231          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpCurrentCartridge" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpCurrentCartridge         | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpEmpty' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_232          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpEmpty" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpEmpty                    | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLetterQuality' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_233          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLetterQuality" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLetterQuality            | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLineChars' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_234          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLineChars" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLineChars                | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLineCharsList' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_235         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLineCharsList" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLineCharsList            | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLineHeight' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_236         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLineHeight" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLineHeight               | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLinesNearEndToEnd' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_237         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLinesNearEndToEnd" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLinesNearEndToEnd        | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLineSpacing' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_238         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLineSpacing" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLineSpacing              | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpLineWidth' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_239         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpLineWidth" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpLineWidth                | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpMaxLines' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_240         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpMaxLines" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpMaxLines                 | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpNearEnd' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_241         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpNearEnd" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpNearEnd                  | false                    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpPrintSide' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_242         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpPrintSide" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpPrintSide                | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpSidewaysMaxChars' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_243         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpSidewaysMaxChars" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | SlpSidewaysMaxChars         | 0                        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @GetProperty
  Scenario: User calls GetProperties API to get value of property 'SlpSidewaysMaxLines' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_244         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call GetProperties API to get "SlpSidewaysMaxLines" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value              |
      | Status Code                 | 200                |
      | Content-Type                | application/json   |
      | Status line                 | HTTP/1.1 200       |
      | SlpSidewaysMaxLines         | 0                  |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |