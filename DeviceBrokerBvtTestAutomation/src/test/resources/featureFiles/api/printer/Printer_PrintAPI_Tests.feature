Feature: Printer Print API Tests

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectPrinter.json"
		
  @BVT @Printer @Connect @REG @Print
  Scenario: User calls Print API with invalid logical name and verify response.
            This scenario covers test cases MAN_PRIN_16          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

      
      When a user call Print API for device with input file "PrintWithinvalidLogicalName.json"
      Then error response includes the following
      | Field                | Value                                    |
      | Status Code          | 400                                      |
      | Content-Type         | application/json                         |
      | Status line          | HTTP/1.1 400                             |
      | type                 | Major                                    |
      | errorCode            | 1105                                     |
      | extendedErrorCode    | 0                                        |
      | message              | PRINTER_DEVICE_NOT_FOUND_ERROR_MESSAGE   |
      
      When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @Connect @REG @Print
  Scenario: User calls Print API with when printer is not in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_19          
    
      When a user call Print API for device with input file "Print.json"
      Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | PRINTER_PRINT_WITH_OUT_CONNECT_REQUEST_MESSAGE       |
      
  @BVT @Printer @Connect @REG @Print
  Scenario: User calls Print API with invalid value for setBitmap command and verify response.
            This scenario covers test cases MAN_PRIN_57          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

      
      When a user call Print API for device with input file "setBitmapWithNumber0.json"
      Then error response includes the following
      | Field                | Value                                                          |
      | Status Code          | 400                                                            |
      | Content-Type         | application/json                                               |
      | Status line          | HTTP/1.1 400                                                   |
      | type                 | Minor                                                          |
      | errorCode            | 1207                                                           |
      | extendedErrorCode    | 0                                                              |
      | message              | PRINTER_PRINT_WITH_SET_BITMAP_NUMBER_ZERO_REQUEST_MESSAGE      |
      
      When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	
  @BVT @Printer @Connect @REG @Print
  Scenario: User calls Print API with invalid value 21 for setBitmap command and verify response.
            This scenario covers test cases MAN_PRIN_54          

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

      
      When a user call Print API for device with input file "setBitmapWithNumber21.json"
      Then error response includes the following
      | Field                | Value                                                          |
      | Status Code          | 400                                                            |
      | Content-Type         | application/json                                               |
      | Status line          | HTTP/1.1 400                                                   |
      | type                 | Minor                                                          |
      | errorCode            | 1207                                                           |
      | extendedErrorCode    | 0                                                              |
      | message              | PRINTER_PRINT_WITH_SET_BITMAP_NUMBER_21_REQUEST_MESSAGE        |
      
      When a user call disconnect API for device with input file "DisconnectPrinter.json"
      Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |