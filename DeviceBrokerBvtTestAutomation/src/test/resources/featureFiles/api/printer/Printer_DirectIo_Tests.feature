Feature: Printer DirectIO Tests

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectPrinter.json"
  
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '1' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_318,MAN_PRIN_319           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "1", data "0" and additionaldata "\u001b|P"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | \|P                      |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '2' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_321           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "2", data "2" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 2                        |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '4' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_323           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "4", data "1" and additionaldata "ABCD"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | 41424344                 |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '5' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_325           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "5", data "0" and additionaldata "C:\\abc.log"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | 433:5<6162632>6<6?67     |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '16' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_327           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "16", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | -11                      |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '17' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_329           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "17", data "-2" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | -2                       |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '18' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_331           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "18", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | -2                       |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '19' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_333           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "19", data "1" and additionaldata "C:\\DeviceBroker\\visualstore.bmp"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '20' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_335           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "20", data "1" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '21' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_337           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "21", data "1" and additionaldata "2048"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | 32303438                 |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '24' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_339           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "24", data "1" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | 43                       |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '45' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_343           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "45", data "0" and additionaldata "1B7E"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | 31423745                 |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '46' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_345           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "46", data "4" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 4                        |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '47' when 'AsyncMode' property is set to 'false'.
            This scenario covers test cases MAN_PRIN_347           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "47", data "0" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '1' when 'AsyncMode' property is set to 'true'.
            This scenario covers test cases MAN_PRIN_349           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "1", data "0" and additionaldata "\u001b|P"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | \|P                      |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '4' when 'AsyncMode' property is set to 'true'.
            This scenario covers test cases MAN_PRIN_351           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "4", data "1" and additionaldata "ABCD"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | 41424344                 |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '5' when 'AsyncMode' property is set to 'true'.
            This scenario covers test cases MAN_PRIN_353           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "5", data "0" and additionaldata "C:\\abc.log"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 0                        |
      | additionalData              | 433:5<6162632>6<6?67     |
      
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '19' when 'AsyncMode' property is set to 'true'.
            This scenario covers test cases MAN_PRIN_355           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "19", data "1" and additionaldata "C:\\DeviceBroker\\visualstore.bmp"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '20' when 'AsyncMode' property is set to 'true'.
            This scenario covers test cases MAN_PRIN_357           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "20", data "1" and additionaldata ""
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | [blank]                  |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @DirectIO
  Scenario: User calls DirectIO API for command number '21' when 'AsyncMode' property is set to 'true'.
            This scenario covers test cases MAN_PRIN_359           

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "AsyncMode" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|

    When a user call DirectIO API for printer with command number "21", data "1" and additionaldata "2048"
    Then response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | data                        | 1                        |
      | additionalData              | 32303438                 |
	    
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |