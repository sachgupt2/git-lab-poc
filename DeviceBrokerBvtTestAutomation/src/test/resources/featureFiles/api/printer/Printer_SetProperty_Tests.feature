Feature: Printer Set Property Tests

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectPrinter.json"
  
  @BVT @Printer @Property @SetProperty
  Scenario: User calls setProperty API for printer device and verify response.
            This scenario covers test cases MAN_PRIN_245, MAN_PRIN_312 and MAN_PRIN_313
    
    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set printer property "RotateSpecial" with value "257"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE  |
      
    When a user call GetProperties API to get "RotateSpecial" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RotateSpecial               | 257                      |
      
     When a user call disconnect API for device with input file "DisconnectPrinter.json"
     Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	
	  When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
	  When a user call GetProperties API to get "RotateSpecial" property values for "POSPrinter" device with terminalId "prime55"
    Then get property response includes the following
      | Field                       | Value                    |
      | Status Code                 | 200                      |
      | Content-Type                | application/json         |
      | Status line                 | HTTP/1.1 200             |
      | RotateSpecial               | 1                        |
	
	  When a user call disconnect API for device with input file "DisconnectPrinter.json"
     Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls setProperty API for printer device with invalid logical name and verify response.
            This scenario covers test cases MAN_PRIN_250
    
    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call setProperties API for device with input file "setPrinterPropertyWithInvalidLogicalName.json"
    Then error response includes the following
      | Field                | Value                                     |
      | Status Code          | 400                                       |
      | Content-Type         | application/json                          |
      | Status line          | HTTP/1.1 400                              |
      | type                 | Major                                     |
      | errorCode            | 1105                                      |
      | extendedErrorCode    | 0                                         |
      | message              | PRINTER_DEVICE_NOT_FOUND_ERROR_MESSAGE    |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
  
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'AsyncMode' with value '1234' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_251         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "AsyncMode" with value "1234"
    Then error response includes the following
      | Field                               | Value                    |
      | Status Code                         | 400                      |
      | Content-Type                        | application/json         |
      | Status line                         | HTTP/1.1 400             |
      | type                                | Minor                    |
      | errorCode                           | 1204                     |
      | extendedErrorCode                   | 0                        |
      | printer-set-invalid-property-value  | AsyncMode                |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
  
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'AsyncMode' with value 'false' when printer is in disconnected (IDLE) state and verify response.
            This scenario covers test cases MAN_PRIN_246, MAN_PRIN_248          

    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then error response includes the following
      | Field                | Value                                                |
      | Status Code          | 400                                                  |
      | Content-Type         | application/json                                     |
      | Status line          | HTTP/1.1 400                                         |
      | type                 | Major                                                |
      | errorCode            | 1305                                                 |
      | extendedErrorCode    | 0                                                    |
      | message              | PRINTER_SET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE  |
	    
	    
	@BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'ConnectionStatus' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_253         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "ConnectionStatus" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | ConnectionStatus         |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
	    
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'AsyncMode' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_254, MAN_PRIN_255         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "AsyncMode" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'CharacterSet' with value '932' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_256         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "CharacterSet" with value "932"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'CharacterSetList' with value '437,932,737,850,999' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_257         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "CharacterSetList" with value "437,932,737,850,999"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | CharacterSetList         |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'CoverOpen' with value 'true' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_258         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "CoverOpen" with value "true"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | CoverOpen                |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'ErrorStation' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_259         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "ErrorStation" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | ErrorStation             |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'FontTypefaceList' with value 'FontA,Condense,FontB' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_260         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "FontTypefaceList" with value "FontA,Condense,FontB"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | FontTypefaceList         |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnCurrentCartridge' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_261         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnCurrentCartridge" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnEmpty' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_262         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnEmpty" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | JrnEmpty                 |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnLetterQuality' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_263         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnLetterQuality" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnLineChars' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_264        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnLineChars" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnLineCharsList' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_265         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnLineCharsList" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | JrnLineCharsList         |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnLineHeight' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_266        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnLineHeight" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnLineSpacing' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_267        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnLineSpacing" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnLineWidth' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_268         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnLineWidth" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | JrnLineWidth             |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'JrnNearEnd' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_269         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "JrnNearEnd" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | JrnNearEnd               |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'MapCharacterSet' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_270        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "MapCharacterSet" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'MapMode' with value '1' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_271        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "MapMode" with value "1"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModeArea' with value 'abc' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_272         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModeArea" with value "abc"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | PageModeArea             |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModeDescriptor' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_273         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModeDescriptor" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | PageModeDescriptor       |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModeHorizontalPosition' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_274        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModeHorizontalPosition" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModePrintArea' with value '00,00,00,00,00,00,00,00' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_275        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModePrintArea" with value "00,00,00,00,00,00,00,00"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModePrintDirection' with value '1' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_276        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModePrintDirection" with value "1"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModeStation' with value '2' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_277        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModeStation" with value "2"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'PageModeVerticalPosition' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_278        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "PageModeVerticalPosition" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecBarCodeRotationList' with value '0,180' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_279         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecBarCodeRotationList" with value "0,180"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecBarCodeRotationList   |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecBitmapRotationList' with value '0,R90,L90,180' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_280         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecBitmapRotationList" with value "0,R90,L90,180"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecBitmapRotationList    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecCartridgeState' with value '268435456' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_281         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecCartridgeState" with value "268435456"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecCartridgeState        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecCurrentCartridge' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_282        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecCurrentCartridge" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecEmpty' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_283         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecEmpty" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecEmpty                 |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLetterQuality' with value 'true' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_284         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLetterQuality" with value "true"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLineChars' with value '32' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_285        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLineChars" with value "32"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLineCharsList' with value '32,41,46' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_286         

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLineCharsList" with value "32,41,46"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecLineCharsList         |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLineHeight' with value '24' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_287        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLineHeight" with value "24"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLineSpacing' with value '30' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_288        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLineSpacing" with value "30"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLinesToPaperCut' with value '7' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_289        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLinesToPaperCut" with value "7"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecLinesToPaperCut       |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecLineWidth' with value '416' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_290        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecLineWidth" with value "416"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecLineWidth             |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecNearEnd' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_291        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecNearEnd" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecNearEnd               |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecSidewaysMaxChars' with value '184' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_292        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecSidewaysMaxChars" with value "184"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecSidewaysMaxChars      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RecSidewaysMaxLines' with value '16' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_293        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RecSidewaysMaxLines" with value "16"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | RecSidewaysMaxLines      |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'RotateSpecial' with value '257' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_294     

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "RotateSpecial" with value "257"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpBarCodeRotationList' with value '' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_295        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpBarCodeRotationList" with value ""
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpBarCodeRotationList   |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpBitmapRotationList' with value '' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_296        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpBitmapRotationList" with value ""
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpBitmapRotationList    |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpCartridgeState' with value '268435456' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_297        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpCartridgeState" with value "268435456"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpCartridgeState        |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpCurrentCartridge' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_298     

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpCurrentCartridge" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpEmpty' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_299        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpEmpty" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpEmpty                 |
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLetterQuality' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_300     

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLetterQuality" with value "false"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLineChars' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_301     

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLineChars" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLineCharsList' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_302        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLineCharsList" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpLineCharsList         |
      
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLineHeight' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_303     

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLineHeight" with value "0"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLinesNearEndToEnd' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_304        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLinesNearEndToEnd" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpLinesNearEndToEnd     |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLineSpacing' with value '10' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_305     

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLineSpacing" with value "10"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_SET_PROPERTY_REQUEST_MESSAGE|
	    
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpLineWidth' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_306        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpLineWidth" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpLineWidth             |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpMaxLines' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_307        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpMaxLines" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpMaxLines              |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpNearEnd' with value 'false' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_308        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpNearEnd" with value "false"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpNearEnd               |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpPrintSide' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_309        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpPrintSide" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpPrintSide             |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpSidewaysMaxChars' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_310        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpSidewaysMaxChars" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpSidewaysMaxChars      |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |
     
  @BVT @Printer @REG @Property @SetProperty
  Scenario: User calls SetProperty API to set property 'SlpSidewaysMaxLines' with value '0' when printer is in connected (BUSY) state and verify response.
            This scenario covers test cases MAN_PRIN_311        

    When a user call connect API for device with input file "ConnectPrinter.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | PRINTER_CONNECT_SUCCESS_MESSAGE  |

    When a user call SetProperty API to set printer property "SlpSidewaysMaxLines" with value "0"
    Then error response includes the following
      | Field                           | Value                    |
      | Status Code                     | 400                      |
      | Content-Type                    | application/json         |
      | Status line                     | HTTP/1.1 400             |
      | type                            | Minor                    |
      | errorCode                       | 1208                     |
      | extendedErrorCode               | 0                        |
      | printer-set-read-only-property  | SlpSidewaysMaxLines      |
      
    When a user call disconnect API for device with input file "DisconnectPrinter.json"
    Then response includes the following
     | Field        | Value                               |
     | Status Code  | 200                                 |
     | Content-Type | application/json                    |
     | Status line  | HTTP/1.1 200                        |
     | message      | PRINTER_DISCONNECT_SUCCESS_MESSAGE  |