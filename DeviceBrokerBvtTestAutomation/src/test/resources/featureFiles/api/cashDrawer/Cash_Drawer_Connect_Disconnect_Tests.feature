Feature: Cash Drawer Connect Disconnect Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
  	
  @Drawer @BVT @Connect
  Scenario: User calls Connect, and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-ConDiscon-1 and DB-CahDrawer-ConDiscon-2
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |
      
  @Drawer @BVT @REG @Connect
  Scenario: User calls disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-ConDiscon-3
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then error response includes the following
      | Field                | Value                                      |
      | Status Code          | 400                                        |
      | Content-Type         | application/json                           |
      | Status line          | HTTP/1.1 400                               |
      | type                 | Major                                      |
      | errorCode            | 1305                                       |
      | extendedErrorCode    | 0                                          |
      | message              | CD_ALREADY_DISCONNECTED_ERROR_MESSAGE      |
      
  