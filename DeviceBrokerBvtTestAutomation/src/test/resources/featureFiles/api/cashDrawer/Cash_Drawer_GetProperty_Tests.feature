Feature: Cash Drawer Get Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
  	
  @Drawer @BVT @Property @GetProperty
  Scenario: User calls Connect, getProperties and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetProp-1
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "all" property values for "Drawer" device with terminalId "prime55"
    Then get property response includes the following
      | Field                | Value                          |
      | Status Code          | 200                            |
      | Content-Type         | application/json               |
      | Status line          | HTTP/1.1 200                   |
      | ConnectionStatus     | Connected                      |
      | DrawerOpened         | false                          |
            
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |
      
  @Drawer @BVT @Property @GetProperty
  Scenario: User calls Connect, getProperty and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetProp-3
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetProperties API to get "DrawerOpened" property values for "Drawer" device with terminalId "prime55"
    Then get property response includes the following
      | Field                | Value                          |
      | Status Code          | 200                            |
      | Content-Type         | application/json               |
      | Status line          | HTTP/1.1 200                   |
      | DrawerOpened         | false                           |
            
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |