Feature: Cash Drawer Capability Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
  	
  @Drawer @BVT @Capability
  Scenario: User calls Connect, getCapabilities and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-2
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "all" capability values for "Drawer" device
    Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapConnection                 | false                          |
     | CapStatus                     | true                           |
     | CapStatusMultiDrawerDetect    | false                          |
      
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |
      
  @Drawer @BVT @Capability
  Scenario: User calls getCapabilities API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-1
   When a user call GetCapabilities API to get "all" capability values for "Drawer" device
    Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapConnection                 | false                          |
     | CapStatus                     | true                           |
     | CapStatusMultiDrawerDetect    | false                          |
     
  @Drawer @BVT  @REG @Capability
  Scenario: User calls getCapabilities API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-3
   When a user call GetCapabilities API to get "CapConnection" capability values for "Drawer" device
   Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapConnection                 | false                          |
     
  @Drawer @BVT  @REG @Capability
  Scenario: User calls getCapabilities API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-4
   When a user call GetCapabilities API to get "CapStatus" capability values for "Drawer" device
   Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapStatus                     | true                           |
     
  @Drawer @BVT  @REG @Capability
  Scenario: User calls getCapabilities API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-5
   When a user call GetCapabilities API to get "CapStatusMultiDrawerDetect" capability values for "Drawer" device
   Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapStatusMultiDrawerDetect    | false                          |
     
  @Drawer @BVT @REG @Capability
  Scenario: User calls Connect, getCapabilities and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-6
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapConnection" capability values for "Drawer" device
    Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapConnection                 | false                          |
      
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |
      
  @Drawer @BVT @REG @Capability
  Scenario: User calls Connect, getCapabilities and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-7
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapStatus" capability values for "Drawer" device
    Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapStatus                     | true                           |
      
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |
      
  @Drawer @BVT @REG @Capability
  Scenario: User calls Connect, getCapabilities and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-GetCap-8
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call GetCapabilities API to get "CapStatusMultiDrawerDetect" capability values for "Drawer" device
    Then get capability response includes the following
     | Field                         | Value                          |
     | Status Code                   | 200                            |
     | Content-Type                  | application/json               |
     | Status line                   | HTTP/1.1 200                   |
     | CapStatusMultiDrawerDetect    | false                          |
      
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |