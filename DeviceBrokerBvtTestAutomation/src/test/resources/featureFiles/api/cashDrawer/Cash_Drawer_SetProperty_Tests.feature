Feature: Cash Drawer Set Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
  	
  @Drawer @BVT @Property @SetProperty
  Scenario: User calls Connect, setProperty and disconnect API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-SetProp-7
    When a user call connect API for device with input file "ConnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | CD_CONNECT_SUCCESS_MESSAGE       |
      
    When a user call SetProperty API to set CashDrawer property "DrawerOpened" with value "true"
    Then error response includes the following
      | Field                | Value                                      |
      | Status Code          | 400                                        |
      | Content-Type         | application/json                           |
      | Status line          | HTTP/1.1 400                               |
      | type                 | Minor                                      |
      | errorCode            | 1208                                       |
      | extendedErrorCode    | 0                                          |
      | message              | CD_SET_READ_ONLY_PROPERTY_MESSAGE          |
            
    When a user call disconnect API for device with input file "DisconnectCashDrawer.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | CD_DISCONNECT_SUCCESS_MESSAGE       |
      
  @Drawer @BVT  @REG @Property @SetProperty
  Scenario: User calls setProperty API for cash drawer device and verify response.
            This scenario covers test cases DB-CahDrawer-SetProp-1
    When a user call SetProperty API to set CashDrawer property "DrawerOpened" with value "true"
    Then error response includes the following
      | Field                | Value                                                   |
      | Status Code          | 400                                                     |
      | Content-Type         | application/json                                        |
      | Status line          | HTTP/1.1 400                                            |
      | type                 | Major                                                   |
      | errorCode            | 1305                                                    |
      | extendedErrorCode    | 0                                                       |
      | message              | CD_SET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE          |