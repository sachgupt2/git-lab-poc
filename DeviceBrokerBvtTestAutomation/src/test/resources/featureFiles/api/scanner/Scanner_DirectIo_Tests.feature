Feature: Scanner DirectIo Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectScanner.json"
  	
  @Scanner @BVT @DirectIO
  Scenario: User calls DirectIO methods for scanner device and verify response.
            This scenario covers following test cases:
            DB-Scanr-DirectIO-1, DB-Scanr-DirectIO-4, DB-Scanr-DirectIO-5
            DB-Scanr-DirectIO-6, DB-Scanr-DirectIO-7, DB-Scanr-DirectIO-8
            DB-Scanr-DirectIO-9
   
    When a user call DirectIO API for Scanner with command number "3", data "0" and additionaldata ""
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
    
    When a user call DirectIO API for Scanner with command number "7", data "0" and additionaldata ""  
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
      
    When a user call DirectIO API for Scanner with command number "5", data "101" and additionaldata "BIG"   
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
    
    When a user call DirectIO API for Scanner with command number "5", data "102" and additionaldata "4300"
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
    
    When a user call DirectIO API for Scanner with command number "4", data "0" and additionaldata ""
    Then response includes the following
      | Field        | Value                                 |
      | Status Code  | 200                                   |
      | Content-Type | application/json                      |
      | Status line  | HTTP/1.1 200                          |
          
    When a user call DirectIO API for Scanner with command number "6", data "101" and additionaldata ""      
    Then response includes the following
      | Field                  | Value                                 |
      | Status Code            | 200                                   |
      | Content-Type           | application/json                      |
      | Status line            | HTTP/1.1 200                          |
      | additionalData         | BIG                                   |
      
    When a user call DirectIO API for Scanner with command number "6", data "102" and additionaldata ""  
    Then response includes the following
      | Field                  | Value                                 |
      | Status Code            | 200                                   |
      | Content-Type           | application/json                      |
      | Status line            | HTTP/1.1 200                          |
      | additionalData         | 4300                                  |