Feature: Scanner Connect Disconnect Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectScanner.json"
  	
  @Scanner @BVT @Connect
  Scenario: User calls Connect, and disconnect API for scanner device and verify response.
            This scenario covers test cases DB-Scanr-ConDiscon-1 and  DB-Scanr-ConDiscon-2
    When a user call connect API for device with input file "ConnectScanner.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | SCANNER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call disconnect API for device with input file "DisconnectScanner.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | SCANNER_DISCONNECT_SUCCESS_MESSAGE  |