Feature: Scanner Get Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectScanner.json"
  	
  @Scanner @BVT @Property @GetProperty
  Scenario: User calls Connect, getProperties and disconnect API for scanner device and verify response.
            This scenario covers test cases DB-Scanr-GetProp-1
    When a user call connect API for device with input file "ConnectScanner.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | SCANNER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call GetProperties API to get "all" property values for "Scanner" device with terminalId "prime55"
    Then get property response includes the following
      | Field                | Value                          |
      | Status Code          | 200                            |
      | Content-Type         | application/json               |
      | Status line          | HTTP/1.1 200                   |
      | ConnectionStatus     | Connected                      |
            
    When a user call disconnect API for device with input file "DisconnectScanner.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | SCANNER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @Scanner @BVT @Property @GetProperty
  Scenario: User calls Connect, getProperties and disconnect API for scanner device and verify response.
            This scenario covers test cases DB-Scanr-GetProp-6
    When a user call connect API for device with input file "ConnectScanner.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | SCANNER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call GetProperties API to get "ConnectionStatus" property values for "Scanner" device with terminalId "prime55"
    Then get property response includes the following
      | Field                | Value                          |
      | Status Code          | 200                            |
      | Content-Type         | application/json               |
      | Status line          | HTTP/1.1 200                   |
      | ConnectionStatus     | Connected                      |
            
    When a user call disconnect API for device with input file "DisconnectScanner.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | SCANNER_DISCONNECT_SUCCESS_MESSAGE  |