Feature: Scanner Capability Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectScanner.json"
  	
  @Scanner @BVT @Capability
  Scenario: User calls getCapabilities API when scanner device is in connected (BUSY) state and verify response.
            This scenario covers test cases DB-Scanr-GetCap-6
    When a user call connect API for device with input file "ConnectScanner.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | SCANNER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call GetCapabilities API to get "all" capability values for "Scanner" device
    Then get capability response includes the following
     | Field            | Value                          |
     | Status Code      | 200                            |
     | Content-Type     | application/json               |
     | Status line      | HTTP/1.1 200                   |
     | CapConnection    | true                           |
      
    When a user call disconnect API for device with input file "DisconnectScanner.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | SCANNER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @Scanner @BVT @Capability
  Scenario: User calls getCapabilities API when scanner device is in disconnected (IDLE) state and verify response.
            This scenario covers test cases DB-Scanr-GetCap-5,DB-Scanr-GetCap-7 
   When a user call GetCapabilities API to get "all" capability values for "Scanner" device
    Then get capability response includes the following
     | Field            | Value                          |
     | Status Code      | 200                            |
     | Content-Type     | application/json               |
     | Status line      | HTTP/1.1 200                   |
     | CapConnection    | true                           |
     
  @Scanner @BVT @REG @Capability
  Scenario: User calls getCapabilities API for scanner device and verify response.
            This scenario covers test cases DB-Scanr-GetCap-9 
   When a user call GetCapabilities API to get "CapConnection" capability values for "Scanner" device
   Then get capability response includes the following
     | Field            | Value                          |
     | Status Code      | 200                            |
     | Content-Type     | application/json               |
     | Status line      | HTTP/1.1 200                   |
     | CapConnection    | true                           |
     
  @Scanner @BVT @REG @Capability
  Scenario: User calls Connect, getCapabilities and disconnect API for scanner device and verify response.
            This scenario covers test cases DB-Scanr-GetCap-10
    When a user call connect API for device with input file "ConnectScanner.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | SCANNER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call GetCapabilities API to get "CapConnection" capability values for "Scanner" device
    Then get capability response includes the following
     | Field            | Value                          |
     | Status Code      | 200                            |
     | Content-Type     | application/json               |
     | Status line      | HTTP/1.1 200                   |
     | CapConnection    | true                           |
      
    When a user call disconnect API for device with input file "DisconnectScanner.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | SCANNER_DISCONNECT_SUCCESS_MESSAGE  |