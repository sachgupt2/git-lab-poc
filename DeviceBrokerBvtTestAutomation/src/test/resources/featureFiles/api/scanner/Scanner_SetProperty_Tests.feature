Feature: Scanner Set Property Tests.

  Background: Device is in disconnected state.
  	When a user call disconnect API for device with input file "DisconnectScanner.json"
  	
  @Scanner @BVT @Property @SetProperty
  Scenario: User calls setProperty API to update read only property.
            This scenario covers test cases DB-Scanr-SetProp-7
    When a user call connect API for device with input file "ConnectScanner.json"
    Then response includes the following
      | Field        | Value                            |
      | Status Code  | 200                              |
      | Content-Type | application/json                 |
      | Status line  | HTTP/1.1 200                     |
      | message      | SCANNER_CONNECT_SUCCESS_MESSAGE  |
      
    When a user call SetProperty API to set Scanner property "ConnectionStatus" with value "true"
    Then error response includes the following
      | Field                | Value                                      |
      | Status Code          | 400                                        |
      | Content-Type         | application/json                           |
      | Status line          | HTTP/1.1 400                               |
      | type                 | Minor                                      |
      | errorCode            | 1208                                       |
      | extendedErrorCode    | 0                                          |
      | message              | SCANNER_SET_READ_ONLY_PROPERTY_MESSAGE     |
            
    When a user call disconnect API for device with input file "DisconnectScanner.json"
    Then response includes the following
      | Field        | Value                               |
      | Status Code  | 200                                 |
      | Content-Type | application/json                    |
      | Status line  | HTTP/1.1 200                        |
      | message      | SCANNER_DISCONNECT_SUCCESS_MESSAGE  |
      
  @Scanner @BVT @Property @SetProperty
  Scenario: User calls Connect, setProperty and disconnect API for scanner device and verify response.
            This scenario covers test cases DB-Scanr-SetProp-1
    When a user call SetProperty API to set Scanner property "ConnectionStatus" with value "true"
    Then error response includes the following
      | Field                | Value                                                   |
      | Status Code          | 400                                                     |
      | Content-Type         | application/json                                        |
      | Status line          | HTTP/1.1 400                                            |
      | type                 | Major                                                   |
      | errorCode            | 1305                                                    |
      | extendedErrorCode    | 0                                                       |
      | message              | SCANNER_SET_PROPERTY_WITH_OUT_CONNECT_ERROR_MESSAGE     |