Feature: Scanner Data Event Tests.
  
    @Sample 
    Scenario: Verify Scan data events for bar code.
    Given Connect to message broker 
    Then Subscribe topic "shared.bus.001.service.ngp.device.scanner.events"
    When a user call connect API for device with input file "ConnectScanner.json"
    Then Request human action: "Scan valid bar code and close this pop up"
    Then In 60 seconds should receive message
    """
    {
        "actions": [{
            "actionType": "scanData",
            "eventBody": {
                "scanData": "K1210729206-MV7",
                "scanDataLabel": "10729206-MV7",
                "scanDataType": "110"
            }
        }]
    }
    """
    Then a user call disconnect API for device with input file "DisconnectScanner.json"
    And Unsubscribe topic "shared.bus.001.service.ngp.device.scanner.events"
    And Disconnect Broker
      