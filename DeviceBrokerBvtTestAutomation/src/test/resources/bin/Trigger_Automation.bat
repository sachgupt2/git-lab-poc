@echo off

SET rootDirectoryUnClean=%~dp0\..

for %%i in ("%rootDirectoryUnClean%") do SET "rootDirectory=%%~fi"

echo location is %rootDirectory%

pushd .

cd /D %rootDirectory%\bin

popd

java -cp %rootDirectory%\lib\* com.cucumber.runner.external.RunCucumberTests --plugin pretty --plugin html:cucumber/html --plugin json:cucumber/json/cucumber.json  --glue com.cucumber %rootDirectory%\featureFiles\api --tags @BVT

rmdir /q /s %rootDirectory%\Report

mkdir %rootDirectory%\Report


REM echo Copying Report to  %rootDirectory%/Report Directory
xcopy /E /I %rootDirectory%\bin\cucumber %rootDirectory%\Report

REM echo Report available in Directory :  %rootDirectory%/Report

REM echo Deleting temp directory %rootDirectory%\bin\cucumber

rmdir /q /s %rootDirectory%\bin\cucumber

REM echo "Report generation code starts here"

echo.
echo  ################## Creating Automation Report ###################
echo.
REM rmdir /q /s %rootDirectory%\input-json

rmdir /q /s %rootDirectory%\output-html-report

REM mkdir %rootDirectory%\input-json

mkdir %rootDirectory%\output-html-report

pushd .

cd /D %rootDirectory%\bin

popd

java -cp %rootDirectory%\lib\* net.masterthought.cucumber.sandwich.CucumberReportMonitor -f %rootDirectory%\Report\json -o %rootDirectory%\output-html-report -n
echo.
echo  ################## Automation Report Location ###################
echo.
echo Report available in Directory :  %rootDirectory%\output-html-report\cucumber-html-reports