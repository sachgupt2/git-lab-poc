package com.cucumber.runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

/**
 * @author gsachin
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = { "pretty", "json:target/cucumber.json", "html:target/html-report","junit:target/cucumber.xml", "rerun:target/rerun.txt" },
		features = {"src/test/resources/featureFiles/api"},
		glue = {"com.cucumber.api.stepdefs" },
		tags =  "@Scanner or @Drawer or @Printer or @MSR",
		dryRun = true)

public class CucumberTest {

	
}
