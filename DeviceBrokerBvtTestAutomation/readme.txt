1. Execute test case from Eclipse:

Open file CucumberTest.java (in package com.cucumber.runner) in eclipse, right click and select Run as Junit Test, I t will execute all the test cases matching the tags specified in tags section.

For example if user need to run Scanner BVT test cases, tags can be mentioned as follows:

@CucumberOptions(
		plugin = { "pretty", "json:target/cucumber.json", "html:target/html-report","junit:target/cucumber.xml", "rerun:target/rerun.txt" },
		features = {"src/test/resources/featureFiles/api"},
		glue = {"com.cucumber.api.stepdefs" },
		tags =  "@Scanner and @BVT",
		dryRun = false)
		
2. Execute test case from command line using maven:

Open command prompt and change to directory where pom.xml file is present and run following command:

mv clean install

It will run all test cases which matches the tags specified in CucumberTest.java (in package com.cucumber.runner).
When execution is complete, it will create html report under DeviceBrokerBvtTestAutomation\target\cucumber-html-reports directory


3. Execute test case from command line using batch file:

Create the build using mvn clean install, it will generate zip file (TestAutomation.zip) under DeviceBrokerBvtTestAutomation\target\package directory.

Copy and extract this file on machine where user need to run automation. (JAVA_HOME should be set in machine).

Open command prompt and navigate to <extracted_dir>\bin and run batch file : Trigger_Automation.bat

When execution is complete, it will create html report under <extracted_dir>\output-html-report\cucumber-html-reports directory